import { appShortTitle } from '../../../lib/messages';
import { TypographyCenter } from '../../Text/Typography';

export const RegisterTeacherPending = () => {
  return (
    <>
      <TypographyCenter variant="h3" gutterBottom>
        Bạn đã đăng ký giảng viên tại {appShortTitle}.
      </TypographyCenter>

      <TypographyCenter gutterBottom>
        Đăng ký của bạn đã được chuyển tới bộ phận xét duyệt
      </TypographyCenter>
      <TypographyCenter gutterBottom>
        Chúng tôi sẽ gửi kết quả tới email của bạn trong 24 giờ
      </TypographyCenter>
    </>
  );
};
