import { CSSProperties } from 'react';

export const NextArrowSlider = (props: {
  className?: 'string';
  style?: CSSProperties;
  onClick?: () => void;
}) => {
  const { className, style, onClick } = props;

  return (
    <div
      className={className}
      style={{ ...style, display: 'block', background: '#00ab5c' }}
      onClick={onClick}
    ></div>
  );
};

export const PrevArrowSlider = (props: {
  className?: 'string';
  style?: CSSProperties;
  onClick?: () => void;
}) => {
  const { className, style, onClick } = props;

  return (
    <div
      className={className}
      style={{ ...style, display: 'block', background: '#00ab5c' }}
      onClick={onClick}
    />
  );
};
