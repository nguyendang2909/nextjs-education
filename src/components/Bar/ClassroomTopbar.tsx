import React from 'react';
import {
  AppBar,
  Box,
  Container,
  Hidden,
  IconButton,
  Link,
  Stack,
  Toolbar,
} from '@mui/material';
import { styled, alpha } from '@mui/system';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { Logo } from '../Logo';
import { AuthMenu } from '../Menu/AuthMenu';
import NextLink from 'next/link';
import { LAYOUT_CONFIG } from '../../config';

const RootStyle = styled(AppBar)(({ theme }) => ({
  backdropFilter: 'blur(6px)',
  WebkitBackdropFilter: 'blur(6px)', // Fix on Mobile
  backgroundColor: alpha(theme.palette.background.default, 0.72),
}));

const ToolbarStyle = styled(Toolbar)(({ theme }) => ({
  minHeight: LAYOUT_CONFIG.classRoom.appBarHeightMobile,
  [theme.breakpoints.up('lg')]: {
    minHeight: LAYOUT_CONFIG.classRoom.appBarHeightDesktop,
    padding: theme.spacing(0, 5),
  },
}));

type TopbarProps = {
  onOpenSidebar: () => void;
};

export const ClassroomTopbar: React.FC<TopbarProps> = ({ onOpenSidebar }) => {
  return (
    <RootStyle>
      <Container maxWidth={false} sx={{ padding: 0 }}>
        <ToolbarStyle>
          <Hidden lgUp>
            <IconButton
              onClick={onOpenSidebar}
              sx={{ mr: 1, color: 'text.primary' }}
            >
              <FontAwesomeIcon icon={faBars} />
            </IconButton>
          </Hidden>

          <Hidden lgDown>
            <NextLink href="/" passHref>
              <Link>
                <Logo />
              </Link>
            </NextLink>
          </Hidden>
          <Box sx={{ flexGrow: 1 }} />

          <Stack
            direction="row"
            alignItems="center"
            spacing={{ xs: 0.5, sm: 1.5 }}
          >
            {/* <Searchbar /> */}

            {/* <CartMenu /> */}
            <AuthMenu />
          </Stack>
        </ToolbarStyle>
      </Container>
    </RootStyle>
  );
};
