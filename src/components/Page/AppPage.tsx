import { Box, Breadcrumbs, Typography } from '@mui/material';
import Head from 'next/head';
import { FC, Fragment, ReactNode } from 'react';
import { messagesService, setMessage } from '../../lib/messages';
import { Breadcrumb, BreadcrumbHome, BreadcrumbProps } from '../Breadcrumbs';
import { ContainerSpacingBottom } from '../Container';
import { StackSpaceBetween } from '../Stack';

type AppPageProps = {
  title: string;
  header?: {
    title?: string;
    breadcrumbs?: BreadcrumbProps[];
    action?: ReactNode;
  };
};

export const AppPage: FC<AppPageProps> = ({ title, header, children }) => {
  return (
    <>
      <Head>
        <title>{messagesService.setPageTitle(title)}</title>
      </Head>

      {!!header && (
        <ContainerSpacingBottom>
          {!!header.breadcrumbs && (
            <Breadcrumbs>
              <BreadcrumbHome />
              {header.breadcrumbs.map((breadcrumbProps, index) => {
                return (
                  <Fragment key={index}>
                    <Breadcrumb {...breadcrumbProps} />
                  </Fragment>
                );
              })}
            </Breadcrumbs>
          )}

          <StackSpaceBetween>
            <Box>
              <Typography variant="h1">
                {setMessage(header.title || title)}
              </Typography>
            </Box>
            {!!header.action && <Box>{header.action}</Box>}
          </StackSpaceBetween>
        </ContainerSpacingBottom>
      )}

      {children}
    </>
  );
};
