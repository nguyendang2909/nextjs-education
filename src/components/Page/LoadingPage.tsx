import Head from 'next/head';
import { FC } from 'react';
import { LoadingContent } from '../../Layout/Content/LoadingContent';
import { setMessage } from '../../lib/messages';

type LoadingPageProps = {
  title?: string;
};

export const LoadingPage: FC<LoadingPageProps> = ({ title }) => {
  return (
    <>
      <Head>
        <title>{title ? <>{setMessage(title)}</> : <>Đang tải</>}</title>
      </Head>

      <LoadingContent />
    </>
  );
};
