import {
  faChalkboardUser,
  faMoneyBillTransfer,
  faSchool,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Avatar, Box, Grid, Stack, Typography } from '@mui/material';
import { FC } from 'react';
import { BoxCenter, BoxSpacing, BoxSpacingBottom } from '.';
import { Messages, setMessage } from '../../lib/messages';

export const ReasonToStudyBox: FC = () => {
  return (
    <Box>
      <BoxSpacingBottom>
        <Typography
          variant="h2"
          sx={{ textAlign: 'center', textTransform: 'uppercase', mb: 2 }}
        >
          3 lý do bạn nên học online tại {setMessage(Messages.app.shortTitle)}
        </Typography>
      </BoxSpacingBottom>
      <BoxSpacing>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={4}>
            <Stack spacing={2} alignItems="center">
              <Avatar sx={{ height: 100, width: 100 }}>
                <FontAwesomeIcon icon={faSchool} size="2x" />
              </Avatar>
              <BoxCenter>
                <Typography variant="h3">Giảng viên uy tín</Typography>
                <Typography variant="body1">Bài giảng chất lượng</Typography>
              </BoxCenter>
            </Stack>
          </Grid>

          <Grid item xs={12} sm={4}>
            <Stack spacing={2} alignItems="center">
              <Avatar sx={{ height: 100, width: 100 }}>
                <FontAwesomeIcon icon={faMoneyBillTransfer} size="2x" />
              </Avatar>
              <BoxCenter>
                <Typography variant="h3">Thanh toán 1 lần</Typography>
                <Typography variant="body1">Học mãi mãi</Typography>
              </BoxCenter>
            </Stack>
          </Grid>

          <Grid item xs={12} sm={4}>
            <Stack spacing={2} alignItems="center">
              <Avatar sx={{ height: 100, width: 100 }}>
                <FontAwesomeIcon icon={faChalkboardUser} size="2x" />
              </Avatar>
              <BoxCenter>
                <Typography variant="h3">Học trực tuyến</Typography>
                <Typography variant="body1">Hỗ trợ trục tuyến</Typography>
              </BoxCenter>
            </Stack>
          </Grid>
        </Grid>
      </BoxSpacing>
    </Box>
  );
};
