import React from 'react';
import { Stack, Button } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGoogle } from '@fortawesome/free-brands-svg-icons';
import { requestService } from '../../lib/request';
import { NextLink } from '../Link';

export default function AuthSocial() {
  return (
    <>
      <Stack direction="row" spacing={2}>
        <NextLink href={requestService.getURL('auth/google')}>
          <Button fullWidth size="large" color="inherit" variant="outlined">
            <FontAwesomeIcon icon={faGoogle} color="#DF3E30" />
          </Button>
        </NextLink>
        <NextLink href={requestService.getURL('auth/facebook')}>
          <Button fullWidth size="large" color="inherit" variant="outlined">
            <FontAwesomeIcon icon={faGoogle} color="#DF3E30" />
          </Button>
        </NextLink>
      </Stack>
    </>
  );
}
