import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { alpha } from '@mui/material';
import { styled } from '@mui/system';

export const FontAwesomeIconSpacing = styled(FontAwesomeIcon)(({ theme }) => ({
  marginRight: theme.spacing(1),
}));

export const IconWrapper = styled('div')(({ theme }) => ({
  margin: 'auto',
  display: 'flex',
  borderRadius: '50%',
  alignItems: 'center',
  width: theme.spacing(8),
  height: theme.spacing(8),
  justifyContent: 'center',
  marginBottom: theme.spacing(1),
  color: theme.palette.warning.dark,
  backgroundImage: `linear-gradient(135deg, ${alpha(
    theme.palette.warning.dark,
    0,
  )} 0%, ${alpha(theme.palette.warning.dark, 0.24)} 100%)`,
}));
