import { IconButton, styled } from '@mui/material';

export const IconButtonMedium = styled(IconButton)(({ theme }) => ({
  width: theme.spacing(5),
  height: theme.spacing(5),
}));
