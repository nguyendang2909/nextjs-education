import { Card, CardContent, styled } from '@mui/material';

export const CardContentCenter = styled(CardContent)(() => ({
  // justifyContent: 'center',
  textAlign: 'center',
}));

export const CardFullHeight = styled(Card)(() => ({
  height: '100%',
}));
