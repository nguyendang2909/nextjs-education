import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faBook, faUserTie } from '@fortawesome/free-solid-svg-icons';
import { APP_URL } from '../config';

export type AccountMenuItem = {
  name: string;
  path: string;
  icon?: IconProp;
  info?: string;
  // children?: AccountMenuItem;
};

export const ACCOUNT_MENU_CONFIG: AccountMenuItem[] = [
  {
    name: 'Quản lý tài khoản',
    icon: faUserTie,
    path: APP_URL.user.management,
  },
  {
    name: 'Khoá học của tôi',
    icon: faBook,
    path: APP_URL.classrooms,
  },
];
