import { CircularProgress, Grid } from '@mui/material';
import { FC } from 'react';

export const LoadingContent: FC = () => {
  return (
    <Grid
      sx={{
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center',
        height: '100%',
        display: 'flex',
      }}
    >
      <CircularProgress />
    </Grid>
  );
};
