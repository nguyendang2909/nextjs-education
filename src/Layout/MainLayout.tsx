import { styled } from '@mui/system';
import React from 'react';
import { useDispatch } from 'react-redux';
import { fetchCourseCategoriesThunk } from '../store/reducers/course-categories.reducer';
import { MainSidebar } from '../components/Bar/MainSidebar';
import { MainTopbar } from '../components/Bar/MainTopbar';
import { MainFooter } from './Footer/MainFooter';
import { Box, Grid } from '@mui/material';

const APP_BAR_MOBILE = 64;

const APP_BAR_DESKTOP = 92;

const RootStyle = styled('div')({
  display: 'flex',
  minHeight: '100vh',
  overflow: 'hidden',
});

const MainStyle = styled(Grid)(({ theme }) => ({
  flexGrow: 1,
  overflow: 'auto',
  minHeight: '100%',
  paddingTop: APP_BAR_MOBILE,
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('lg')]: {
    paddingTop: APP_BAR_DESKTOP,
  },
}));

export const MainLayout: React.FC = ({ children }) => {
  const [isOpenSidebar, setSidebar] = React.useState<boolean>(false);

  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(fetchCourseCategoriesThunk());
  }, [dispatch]);

  const handleSetOpenSidebar = () => {
    setSidebar(prev => !prev);
  };

  const handleCloseSidebar = () => {
    setSidebar(false);
  };

  return (
    <RootStyle>
      <MainTopbar onOpenSidebar={handleSetOpenSidebar} />
      <MainSidebar
        isOpenSidebar={isOpenSidebar}
        onCloseSidebar={handleCloseSidebar}
      />
      <MainStyle container direction="column">
        <Box
          sx={{
            paddingTop: 2,
            flexGrow: 1,
            width: '100%',
            overflowX: 'hidden',
          }}
        >
          {children}
        </Box>
        <MainFooter />
      </MainStyle>
    </RootStyle>
  );
};
