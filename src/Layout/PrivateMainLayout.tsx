import { styled } from '@mui/system';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { fetchCourseCategoriesThunk } from '../store/reducers/course-categories.reducer';
import { MainSidebar } from '../components/Bar/MainSidebar';
import { MainTopbar } from '../components/Bar/MainTopbar';
import { MainFooter } from './Footer/MainFooter';
import { Box, Grid } from '@mui/material';
import { useAppSelector } from '../store/hooks';
import { useRouter } from 'next/router';
import { APP_URL } from '../config';

const APP_BAR_MOBILE = 64;

const APP_BAR_DESKTOP = 92;

const RootStyle = styled('div')({
  display: 'flex',
  minHeight: '100vh',
  overflow: 'hidden',
});

const MainStyle = styled(Grid)(({ theme }) => ({
  flexGrow: 1,
  overflow: 'auto',
  minHeight: '100%',
  paddingTop: APP_BAR_MOBILE,
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('lg')]: {
    paddingTop: APP_BAR_DESKTOP,
  },
}));

export const PrivateMainLayout: React.FC = ({ children }) => {
  const [isOpenSidebar, setSidebar] = React.useState<boolean>(false);

  const router = useRouter();

  const logged = useAppSelector(state => state.user?.logged);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCourseCategoriesThunk());
  }, [dispatch]);

  useEffect(() => {
    if (logged === false) {
      router.replace({
        pathname: APP_URL.login,
        query: {
          redirect: router.asPath,
        },
      });
    }
  }, [logged, router]);

  const handleSetOpenSidebar = () => {
    setSidebar(prev => !prev);
  };

  const handleCloseSidebar = () => {
    setSidebar(false);
  };

  if (!logged) {
    return <></>;
  }

  return (
    <RootStyle>
      <MainTopbar onOpenSidebar={handleSetOpenSidebar} />
      <MainSidebar
        isOpenSidebar={isOpenSidebar}
        onCloseSidebar={handleCloseSidebar}
      />
      <MainStyle container direction="column">
        <Box
          sx={{
            paddingTop: 2,
            flexGrow: 1,
            width: '100%',
            overflowX: 'hidden',
          }}
        >
          {children}
        </Box>
        <MainFooter />
      </MainStyle>
    </RootStyle>
  );
};
