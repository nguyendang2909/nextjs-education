import React, { FC, useEffect } from 'react';
import { styled } from '@mui/material/styles';
import { useAppSelector } from '../store/hooks';
import { TeacherSidebar } from '../components/Bar/TeacherSidebar';
import { TeacherTopbar } from '../components/Bar/TeacherTopbar';
import { ERole } from '../types/enums';
import { useRouter } from 'next/router';
import { APP_URL } from '../config';

const APP_BAR_MOBILE = 64;
const APP_BAR_DESKTOP = 92;

const RootStyle = styled('div')({
  display: 'flex',
  minHeight: '100vh',
  overflow: 'hidden',
});

const MainStyle = styled('div')(({ theme }) => ({
  flexGrow: 1,
  overflow: 'auto',
  minHeight: '100%',
  paddingTop: APP_BAR_MOBILE + 24,
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('lg')]: {
    paddingTop: APP_BAR_DESKTOP + 24,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
}));

export const TeacherLayout: FC = ({ children }) => {
  const router = useRouter();

  const [open, setOpen] = React.useState(false);

  const logged = useAppSelector(state => state.user?.logged);

  const userRole = useAppSelector(state => state.user?.info?.role);

  useEffect(() => {
    switch (logged) {
      case true:
        if (!userRole || userRole !== ERole.Teacher) {
          router.replace({
            pathname: APP_URL.notFound,
          });
        }
        break;

      case false:
        router.replace({
          pathname: APP_URL.login,
          query: {
            redirect: router.asPath,
          },
        });
        break;

      default:
        break;
    }
  }, [logged, router, userRole]);

  const handleSetOpen = () => {
    setOpen(prev => !prev);
  };

  const handleClose = () => {
    setOpen(false);
  };

  if (!logged) {
    return <></>;
  }

  if (!userRole || userRole !== ERole.Teacher) {
    return <></>;
  }

  return (
    <>
      <RootStyle>
        <TeacherTopbar onOpenSidebar={handleSetOpen} />
        <TeacherSidebar isOpenSidebar={open} onCloseSidebar={handleClose} />
        <MainStyle>{children}</MainStyle>
      </RootStyle>
    </>
  );
};
