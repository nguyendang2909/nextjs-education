import React, { FC, useEffect } from 'react';
import { styled } from '@mui/material/styles';
import { useAppSelector } from '../store/hooks';
import { TeacherSidebar } from '../components/Bar/TeacherSidebar';
import { TeacherTopbar } from '../components/Bar/TeacherTopbar';
import { MainTopbar } from '../components/Bar/MainTopbar';
import { MainSidebar } from '../components/Bar/MainSidebar';
import { ClassroomTopbar } from '../components/Bar/ClassroomTopbar';
import { APP_URL, LAYOUT_CONFIG } from '../config';
import { Box } from '@mui/material';
import { MainFooter } from './Footer/MainFooter';
import { ClassroomFooter } from './Footer/ClassroomFooter';
import { useRouter } from 'next/router';

const APP_BAR_MOBILE = 64;
const APP_BAR_DESKTOP = 92;

const RootStyle = styled('div')({
  display: 'flex',
  minHeight: '100vh',
  overflow: 'hidden',
});

const MainStyle = styled('div')(({ theme }) => ({
  flexGrow: 1,
  overflow: 'auto',
  minHeight: '100%',
  paddingTop: LAYOUT_CONFIG.classRoom.appBarHeightMobile,
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('lg')]: {
    paddingTop: LAYOUT_CONFIG.classRoom.appBarHeightDesktop + 16,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
}));

export const ClassroomLayout: FC = ({ children }) => {
  const [isOpenSidebar, setSidebar] = React.useState<boolean>(false);

  const router = useRouter();

  const userId = useAppSelector(state => state.user?.info?.id);

  const logged = useAppSelector(state => state.user?.logged);

  const handleSetOpenSidebar = () => {
    setSidebar(prev => !prev);
  };

  const handleCloseSidebar = () => {
    setSidebar(false);
  };

  useEffect(() => {
    switch (logged) {
      case false:
        router.replace({
          pathname: APP_URL.login,
          query: {
            redirect: router.asPath,
          },
        });
        break;

      default:
        break;
    }
  }, [logged]);

  return (
    <>
      {userId ? (
        <RootStyle>
          <ClassroomTopbar onOpenSidebar={handleSetOpenSidebar} />
          <MainSidebar
            isOpenSidebar={isOpenSidebar}
            onCloseSidebar={handleCloseSidebar}
          />
          <MainStyle>
            <Box
              sx={{
                paddingTop: 2,
                flexGrow: 1,
                width: '100%',
                overflowX: 'hidden',
              }}
            >
              {children}
            </Box>
            <ClassroomFooter />
          </MainStyle>
        </RootStyle>
      ) : (
        <></>
      )}
    </>
  );
};
