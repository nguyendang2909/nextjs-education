import { APP_API } from '../config';
import { OrderData } from '../types/fetch-data.type';
import { CreateOrderDto } from '../types/request.dto';
import { requestAPI } from './request';

class OrdersService {
  async create(createOrderDto: CreateOrderDto): Promise<OrderData> {
    return await requestAPI.post<OrderData>(APP_API.orders, createOrderDto);
  }

  // async getMany(params: FindManyCartsDto = {}): Promise<CartItemData[]> {
  //   return await requestAPI.get<CartItemData[]>(APP_API.carts, {
  //     params,
  //   });
  // }

  // async getPrice(params: FindAllCartsDto = {}): Promise<CartPriceData> {
  //   return await requestAPI.get<CartPriceData>(APP_API.cartPrice, {
  //     params,
  //   });
  // }

  // async count(params: FindAllCartsDto = {}) {
  //   return await requestAPI.get<number>(APP_API.count.carts, {
  //     params,
  //   });
  // }

  async getOneById(id: number): Promise<OrderData> {
    return await requestAPI.get<OrderData>(`${APP_API.orders}/${id}`);
  }

  // getIdFromParamId(paramId: string): number {
  //   const splitedParamId = paramId.split(courseParamSplitter);

  //   return +splitedParamId[splitedParamId.length - 1];
  // }

  // async delete(cartItemId: number): Promise<void> {
  //   await requestAPI.delete(`/carts/${cartItemId}`);
  // }
}

export const ordersService = new OrdersService();
