import { APP_API, APP_URL, courseParamSplitter } from '../config';
import { CourseData } from '../types/fetch-data.type';
import {
  CourseUnlockDto,
  FindAllCoursesDto,
  FindManyCoursesDto,
} from '../types/request.dto';
import { requestAPI } from './request';

class CoursesService {
  async getMany(params: FindManyCoursesDto = {}): Promise<CourseData[]> {
    const result = await requestAPI.get<CourseData[]>('/courses', {
      params,
    });

    return result;
  }

  async count(params: FindAllCoursesDto = {}) {
    return await requestAPI.get<number>('/courses/count', {
      params,
    });
  }

  async getOneById(id: number): Promise<CourseData> {
    return await requestAPI.get<CourseData>(`/courses/${id}`);
  }

  async learnById(id: number): Promise<CourseData> {
    return await requestAPI.get<CourseData>(`${APP_API.learnCourse}/${id}`);
  }

  async unlock(courseUnlockDto: CourseUnlockDto): Promise<CourseData> {
    return await requestAPI.post<CourseData>(
      `${APP_API.courseUnlock}`,
      courseUnlockDto,
    );
  }

  getPageLinkFromCourseIdAndName(id: number, name?: string): string {
    return `/${name?.replaceAll(' ', '-')}${courseParamSplitter}${id}`;
  }

  getIdFromParamId(paramId: string): number {
    const splitedParamId = paramId.split(courseParamSplitter);

    return +splitedParamId[splitedParamId.length - 1];
  }

  getLearnLink(id: number) {
    return `${APP_URL.classrooms}/${id}`;
  }
}

export const coursesService = new CoursesService();
