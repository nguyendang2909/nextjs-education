import {
  faBan,
  faCheck,
  faDoorOpen,
  faTrash,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Typography, TableCell, Box } from '@mui/material';
import { useRouter } from 'next/router';
import { FC } from 'react';
import { MoreMenu } from '../../components/Menu/MoreMenu';
import { StackSpaceBetween } from '../../components/Stack';
import { TablePopoverFilter } from '../../components/Table/TablePopoverFilter';
import { Messages, setMessage } from '../../lib/messages';
import { urlQueryService } from '../../lib/url-query.service';

type TableHeaderCellRecordStatusProps = {};

export const TableHeaderCellRecordStatus: FC<
  TableHeaderCellRecordStatusProps
> = () => {
  const router = useRouter();

  const isActive = urlQueryService.getOne('isActive', { query: router.query });

  return (
    <>
      <TableCell sx={{ width: '140px', maxWidth: '140px' }}>
        <StackSpaceBetween>
          {setMessage(Messages.common.record)}
          <Box>
            <TablePopoverFilter
              urlQueryName="isActive"
              options={[
                {
                  text: Messages.common.exist,
                  value: 'true',
                },
                {
                  text: Messages.action.deleted,
                  value: 'false',
                },
              ]}
            />
          </Box>
        </StackSpaceBetween>
      </TableCell>
    </>
  );
};

type TableCellRecordStatusProps = {
  isActive?: boolean;
  id: number;
  onChange: (id: number, { isActive }: { isActive?: boolean }) => Promise<void>;
  refetch: () => void;
};

export const TableCellRecordStatus: FC<TableCellRecordStatusProps> = ({
  isActive,
  id,
  onChange,
  refetch,
}) => {
  const handleChangeRecordStatus = async (value: boolean) => {
    try {
      await onChange(id, {
        isActive: value,
      });

      refetch();
    } catch (err) {}
  };

  return (
    <TableCell>
      <StackSpaceBetween>
        <Typography sx={{ color: isActive ? 'primary.main' : 'error.main' }}>
          {isActive !== undefined ? (
            <>
              <FontAwesomeIcon icon={isActive ? faCheck : faBan} />
            </>
          ) : (
            <></>
          )}
        </Typography>
        <Box>
          <MoreMenu
            items={[
              isActive
                ? {
                    name: Messages.action.delete,
                    icon: faTrash,
                    onClick: () => {
                      handleChangeRecordStatus(false);
                    },
                  }
                : {
                    name: Messages.action.active,
                    icon: faDoorOpen,
                    onClick: () => {
                      handleChangeRecordStatus(true);
                    },
                  },
            ]}
          ></MoreMenu>
        </Box>
      </StackSpaceBetween>
    </TableCell>
  );
};
