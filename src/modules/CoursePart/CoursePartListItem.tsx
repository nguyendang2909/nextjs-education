import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Collapse,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import { useState } from 'react';
import { Messages, setMessage } from '../../lib/messages';
import { CoursePartData } from '../../types/fetch-data.type';
import { LessonListItem } from '../Lesson/LessonListItem';

type CoursePartListItemProps = {
  data: CoursePartData;
  number: number;
};

export const CoursePartListItem: React.FC<CoursePartListItemProps> = props => {
  const { data, number } = props;

  const [isExpand, setExpand] = useState<boolean>(false);

  const { name, lesson } = data;

  const handleSetExpand = () => {
    setExpand(!isExpand);
  };

  return (
    <>
      <ListItemButton onClick={handleSetExpand}>
        <ListItemIcon>
          {isExpand ? (
            <FontAwesomeIcon icon={faAngleDown} />
          ) : (
            <FontAwesomeIcon icon={faAngleUp} />
          )}
        </ListItemIcon>
        <ListItemText
          primaryTypographyProps={{ fontWeight: 'bold' }}
          primary={`${setMessage(Messages.course.part)} ${number}: ${setMessage(
            name,
          )}`}
        />
      </ListItemButton>
      {!!lesson && lesson.length > 0 && (
        <Collapse in={isExpand} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {lesson.map((item, index) => {
              return <LessonListItem key={index} data={item} />;
            })}
          </List>
        </Collapse>
      )}
    </>
  );
};
