import {
  faFile,
  faAngleDown,
  faAngleUp,
  faCirclePlus,
  faBook,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Box,
  Button,
  Collapse,
  Divider,
  Grid,
  IconButton,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Stack,
  TableCell,
  TableRow,
  Typography,
} from '@mui/material';
import { Fragment, useState } from 'react';
import { Messages, setMessage } from '../../../lib/messages';
import { CoursePartData } from '../../../types/fetch-data.type';
import { EditLessons } from '../../Lesson/Edit/EditLessons';
import { MoreMenu } from '../../../components/Menu/MoreMenu';
import { StackSpaceBetween } from '../../../components/Stack';

type EditCoursePartProps = {
  coursePart: CoursePartData;
  onEdit: (id: number) => void;
  onDelete: (coursePart: CoursePartData) => void;
  onCreate: (coursePart: CoursePartData) => void;
  onFinish: () => void;
  selectedLessonId?: number;
};

export const EditCoursePart: React.FC<EditCoursePartProps> = ({
  coursePart,
  onEdit,
  onDelete,
  onCreate,
  onFinish,
  selectedLessonId,
}) => {
  const {
    id: coursePartId,
    orderPosition: coursePartOrderPosition,
    name: coursePartName,
    lesson: lessons,
  } = coursePart;

  const [open, setOpen] = useState<boolean>(
    !!selectedLessonId &&
      !!lessons?.find(lesson => lesson.id === selectedLessonId),
  );

  const handleSetOpen = () => {
    setOpen(!open);
  };

  return (
    <Fragment>
      <Box>
        <StackSpaceBetween>
          <Grid item>
            <ListItemButton onClick={handleSetOpen} sx={{ paddingLeft: 0 }}>
              <ListItemIcon>
                <IconButton size="small">
                  <FontAwesomeIcon icon={open ? faAngleUp : faAngleDown} />
                </IconButton>
              </ListItemIcon>
              <ListItemText>
                <Typography>
                  <FontAwesomeIcon icon={faBook} /> {coursePartOrderPosition}.
                  {` ${setMessage(coursePartName)}`}
                </Typography>
              </ListItemText>
            </ListItemButton>
          </Grid>
          <Grid item>
            <MoreMenu
              color="primary"
              items={[
                {
                  name: Messages.action.edit,
                  icon: 'edit',
                  onClick: () => {
                    coursePartId && onEdit(coursePartId);
                  },
                },
                {
                  name: Messages.action.delete,
                  icon: 'trash',
                  onClick: () => {
                    onDelete(coursePart);
                  },
                },
              ]}
            />
          </Grid>
        </StackSpaceBetween>

        <Collapse in={open} timeout="auto" unmountOnExit>
          <Box pl={5} pr={2}>
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="space-between"
              mb={2}
              mr={5}
            >
              <Typography gutterBottom>
                {/* {setMessage(Messages.course.lesson)} */}
              </Typography>
              <Button
                onClick={() => {
                  onCreate(coursePart);
                }}
                startIcon={<FontAwesomeIcon icon={faCirclePlus} />}
              >
                {setMessage(`${Messages.course.lesson} ${Messages.common.new}`)}
              </Button>
            </Stack>

            <EditLessons lessons={lessons} onFinish={onFinish} />
          </Box>
        </Collapse>

        <Divider />
      </Box>
    </Fragment>
  );
};
