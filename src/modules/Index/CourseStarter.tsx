import { faArrowRight, faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Box, Button, Grid, Stack, Typography } from '@mui/material';
import { FC } from 'react';
import { BoxBackground } from '../../components/Box';
import { ButtonLink } from '../../components/Button/ButtonLink';
import { NextLink } from '../../components/Link';
import { APP_URL } from '../../config';
import { Messages, setMessage } from '../../lib/messages';

export const CourseStarter: FC = () => {
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <BoxBackground
            sx={{
              backgroundImage: 'url("/static/images/banners/student.jpg")',
              width: '100%',
              height: '100%',
            }}
          ></BoxBackground>
        </Grid>
        <Grid item xs={12} md={6}>
          <Box sx={{ pt: 7, pb: 7 }}>
            <Typography variant="h1" gutterBottom>
              Học online,
              <br />
              dành cho mỗi người.
              <br />
              Kết quả thực tế.
            </Typography>
            <Typography gutterBottom>
              Sứ mệnh cung cấp nền giáo dục miễn phí, đẳng cấp thế giới cho mọi
              người, ở bất kỳ đâu.
            </Typography>
            <Stack direction="row" spacing={2}>
              <ButtonLink
                href={APP_URL.classrooms}
                variant="contained"
                size="large"
                color="primary"
                startIcon={<FontAwesomeIcon icon={faArrowRight} />}
              >
                Bắt đầu học
              </ButtonLink>
              <ButtonLink
                href={APP_URL.courses}
                variant="outlined"
                size="large"
                color="primary"
                startIcon={<FontAwesomeIcon icon={faSearch} />}
              >
                {setMessage(Messages.action.search)}
              </ButtonLink>
            </Stack>
          </Box>
        </Grid>
      </Grid>
    </>
  );
};
