import {
  Avatar,
  Box,
  Card,
  CardActionArea,
  CardContent,
  useTheme,
} from '@mui/material';
import { FC, Fragment } from 'react';
import Slider from 'react-slick';
import { BoxSpacingTop } from '../../components/Box';
import { NextLink } from '../../components/Link';
import {
  NextArrowSlider,
  PrevArrowSlider,
} from '../../components/Slider/arrow';
import {
  TypographyOneLine,
  TypographyTwoLine,
} from '../../components/Text/Typography';
import { setMessage } from '../../lib/messages';
import { requestService } from '../../lib/request';
import { specialCharacters } from '../../lib/special-characters';
import { teachersService } from '../../lib/teachers.service';
import { UserData } from '../../types/fetch-data.type';

type TeachersSliderProps = {
  teachers: UserData[];
};

export const TeachersSlider: FC<TeachersSliderProps> = ({ teachers }) => {
  const theme = useTheme();

  return (
    <Slider
      dots={false}
      infinite={false}
      speed={500}
      slidesToShow={3.7}
      slidesToScroll={1}
      initialSlide={0}
      arrows={true}
      nextArrow={<NextArrowSlider />}
      prevArrow={<PrevArrowSlider />}
      responsive={[
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2.7,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1.7,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1.3,
          },
        },
      ]}
    >
      {teachers.map((teacher, index) => {
        return (
          <Fragment key={index}>
            {!!teacher.id && (
              <Box sx={{ padding: theme.spacing(2, 1) }}>
                <Card>
                  <CardActionArea>
                    <NextLink
                      href={teachersService.getPageLinkFromIdAndName(
                        teacher.id,
                        teacher.displayName,
                      )}
                      passHref
                    >
                      <CardContent>
                        <Avatar
                          sx={{
                            width: '150px',
                            height: '150px',
                            margin: '0 auto',
                          }}
                          src={
                            teacher.displayAvatarURL
                              ? requestService.getURL(teacher.displayAvatarURL)
                              : undefined
                          }
                        >
                          {!!teacher.displayName
                            ? teacher.displayName[0]
                            : undefined}
                        </Avatar>
                        <BoxSpacingTop>
                          <TypographyOneLine
                            sx={{
                              color: '#273167',
                              textAlign: 'center',
                              fontWeight: 700,
                            }}
                          >
                            {setMessage(teacher.displayName) ||
                              specialCharacters.space}
                          </TypographyOneLine>
                          <TypographyTwoLine
                            sx={{
                              textAlign: 'center',
                              lineHeight: '1.5em',
                              height: '3em',
                              color: 'text.secondary',
                            }}
                          >
                            {setMessage(teacher.title) ||
                              specialCharacters.space}
                          </TypographyTwoLine>
                        </BoxSpacingTop>
                      </CardContent>
                    </NextLink>
                  </CardActionArea>
                </Card>
              </Box>
            )}
          </Fragment>
        );
      })}
    </Slider>
  );
};
