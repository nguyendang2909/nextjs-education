import React, { FC, useState } from 'react';
import { toast } from 'react-toastify';
import { Messages } from '../../../lib/messages';
import { teacherLessonsService } from '../../../lib/teacher-lessons.service';
import { LessonData } from '../../../types/fetch-data.type';
import { DeleteDialog } from '../../../components/Dialog/DeleteDialog';
import { EditLesson } from './EditLesson';

type EditLessonsProps = {
  lessons?: LessonData[];
  onFinish: () => void;
};

export const EditLessons: FC<EditLessonsProps> = ({
  lessons = [],
  onFinish,
}) => {
  const [deleteLesson, setDeleteLesson] = useState<LessonData>({});

  const handleCloseDeleteLesson = () => {
    setDeleteLesson({});
  };

  const handleDeleteLesson = async () => {
    try {
      const deleteLessonId = deleteLesson.id;

      if (deleteLessonId) {
        await teacherLessonsService.delete(deleteLessonId);
      }
    } catch (err) {
      if (err instanceof Error) {
        toast.error(err.message);
      }
    }
  };

  return (
    <>
      {lessons.map((lesson, index) => {
        return (
          <EditLesson key={index} lesson={lesson} onDelete={setDeleteLesson} />
        );
      })}
      <DeleteDialog
        name={`${Messages.lesson.name} ${deleteLesson.name || ''}`}
        open={!!deleteLesson.id}
        onClose={handleCloseDeleteLesson}
        onDelete={handleDeleteLesson}
        onFinish={onFinish}
      />
    </>
  );
};
