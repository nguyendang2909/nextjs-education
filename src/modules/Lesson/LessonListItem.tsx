import { faFileLines } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ListItem, ListItemIcon, ListItemText } from '@mui/material';
import { Messages, setMessage } from '../../lib/messages';
import { LessonData } from '../../types/fetch-data.type';

type LessonListItemProps = {
  data: LessonData;
};

export const LessonListItem: React.FC<LessonListItemProps> = props => {
  const { data } = props;

  const { name, number } = data;

  return (
    <ListItem divider sx={{ pl: 4 }}>
      <ListItemIcon>
        <FontAwesomeIcon icon={faFileLines} />
      </ListItemIcon>
      <ListItemText
        primary={`${setMessage(Messages.course.lesson)} ${
          number || ''
        }: ${setMessage(name)}`}
      />
      Học thử
    </ListItem>
  );
};
