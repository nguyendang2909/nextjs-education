import {
  faCalendarCheck,
  faCartPlus,
  faCirclePlay,
  faClock,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Button,
  Card,
  CardContent,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Theme,
  Typography,
  SxProps,
  Stack,
  CardMedia,
  Box,
} from '@mui/material';
import { Formatter } from '../../lib/formatter';
import { Messages, setMessage } from '../../lib/messages';
import NextLink from 'next/link';
import { CoursePurchaseData } from '../../types/fetch-data.type';
import { classroomsService } from '../../lib/classrooms.service';
import { requestAPI, requestService } from '../../lib/request';
import { FC } from 'react';
import { CourseLearnNav } from './CourseLearnNav';

type CourseLearnCardProps = {
  courseId: number;
  refetch: () => void;
  paid?: boolean;
  countLessons?: number;
  countStudents?: number;
  duration?: number;
  price?: number;
  promotionPrice?: number;
  sx?: SxProps<Theme>;
  updatedAt?: string;
  coverImageURL?: string;
  onClickAddToCart: () => void;
  onClickRegisterToLearn: () => void;
};

export const CourseLearnCard: FC<CourseLearnCardProps> = ({
  price,
  promotionPrice,
  courseId,
  refetch,
  paid,
  sx = {},
  duration: courseDuration,
  updatedAt: courseUpdatedAt,
  countLessons: courseCountLessons,
  countStudents,
  coverImageURL: courseCoverImageURL,
  onClickAddToCart,
  onClickRegisterToLearn,
}) => {
  return (
    <Card sx={{ width: '350px', ...sx }}>
      <CardMedia
        component={Box}
        sx={{
          height: '200px',
          backgroundImage: courseCoverImageURL
            ? `url("${requestService.getURL(courseCoverImageURL)}")`
            : undefined,
        }}
      ></CardMedia>
      <CardContent>
        {promotionPrice !== undefined ? (
          <Box component="div" sx={{ mb: 2 }}>
            <Typography
              component="span"
              sx={{ fontSize: 24, mr: 1, fontWeight: 'bold' }}
            >
              {Formatter.formatMoney(promotionPrice)}
            </Typography>
            <Typography
              component="span"
              sx={{ fontSize: 16, textDecoration: 'line-through' }}
            >
              {Formatter.formatMoney(price)}
            </Typography>
          </Box>
        ) : (
          <Typography sx={{ fontSize: 24, mr: 1, fontWeight: 'bold' }}>
            {Formatter.formatMoney(price)}
          </Typography>
        )}

        <CourseLearnNav
          paid={!!paid}
          onClickRegisterToLearn={onClickRegisterToLearn}
          onClickAddToCart={onClickAddToCart}
          price={price}
          promotionPrice={promotionPrice}
        />

        <List>
          {countStudents && countStudents > 0 ? (
            <ListItem>
              <ListItemIcon>
                <FontAwesomeIcon icon={faCirclePlay} />
              </ListItemIcon>
              <ListItemText>
                <Typography component="span">
                  {countStudents} học viên
                </Typography>
              </ListItemText>
            </ListItem>
          ) : (
            <></>
          )}

          {courseDuration && courseDuration > 0 ? (
            <ListItem>
              <ListItemIcon>
                <FontAwesomeIcon icon={faClock} />
              </ListItemIcon>
              <ListItemText>
                <Typography>{courseDuration} phút</Typography>
              </ListItemText>
            </ListItem>
          ) : (
            <></>
          )}

          {courseCountLessons && courseCountLessons > 0 ? (
            <ListItem>
              <ListItemIcon>
                <FontAwesomeIcon icon={faCirclePlay} />
              </ListItemIcon>
              <ListItemText>
                <Typography component="span" sx={{ fontWeight: 'bold' }}>
                  {`${courseCountLessons} bài giảng`}
                </Typography>
              </ListItemText>
            </ListItem>
          ) : (
            <></>
          )}

          {/* <ListItem>
            <ListItemIcon>
              <FontAwesomeIcon icon={faCertificate} />
            </ListItemIcon>
            <ListItemText>
              <Typography>Sở hữu khoá học trọn đời</Typography>
            </ListItemText>
          </ListItem> */}

          {/* <ListItem>
            <ListItemIcon>
              <FontAwesomeIcon icon={faPercent} />
            </ListItemIcon>
            <ListItemText>
              <Typography>Cấp chứng nhận hoàn thành</Typography>
            </ListItemText>
          </ListItem> */}

          {/* <ListItem>
            <ListItemIcon>
              <FontAwesomeIcon icon={faPercent} />
            </ListItemIcon>
            <ListItemText>
              <Typography>
                Giảm thêm <b>20k</b> khi thanh toán online
              </Typography>
            </ListItemText>
          </ListItem> */}

          {courseUpdatedAt && (
            <ListItem>
              <ListItemIcon>
                <FontAwesomeIcon icon={faCalendarCheck} />
              </ListItemIcon>
              <ListItemText>
                <Typography>
                  Cập nhật: {Formatter.formatTimeToDate(courseUpdatedAt)}
                </Typography>
              </ListItemText>
            </ListItem>
          )}
        </List>
      </CardContent>
    </Card>
  );
};
