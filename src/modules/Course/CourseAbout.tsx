import { Box, Card, CardContent } from '@mui/material';
import React from 'react';
import { TextEditorReadOnly } from '../../components/Editor/TextEditorReadOnly';
import { TypographyBorderBottom } from '../../components/Text/Typography';
import { BoxSpacingBottom } from '../../components/Box';

type CourseAboutCardProps = {
  courseAbout: string;
};

export const CourseAboutCard: React.FC<CourseAboutCardProps> = ({
  courseAbout,
}) => {
  return (
    <Card>
      <CardContent>
        <Box sx={{ mb: 3 }}>
          <TypographyBorderBottom variant="h2">
            Giới thiệu khoá học
          </TypographyBorderBottom>
        </Box>
        <TextEditorReadOnly data={courseAbout} />
      </CardContent>
    </Card>
  );
};
