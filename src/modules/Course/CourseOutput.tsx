import { faCheck, faCircleCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Card,
  CardContent,
  CardHeader,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import React from 'react';
import { BoxSpacingBottom } from '../../components/Box';
import { TypographyBorderBottom } from '../../components/Text/Typography';

type CourseOutputCardProps = {
  courseOutput: string;
};

export const CourseOutputCard: React.FC<CourseOutputCardProps> = ({
  courseOutput,
}) => {
  return (
    <Card>
      <CardContent>
        <BoxSpacingBottom>
          <TypographyBorderBottom variant="h2">
            Lợi ích từ khoá học
          </TypographyBorderBottom>
        </BoxSpacingBottom>
        <CourseOutputGrid courseOutput={courseOutput} />
      </CardContent>
    </Card>
  );
};

type CourseOutputGridProps = {
  courseOutput: string;
};

export const CourseOutputGrid: React.FC<CourseOutputGridProps> = ({
  courseOutput,
}) => {
  const items = courseOutput.split('\n');

  if (!items || !items.length) {
    return <></>;
  }

  return (
    <List
      component={Grid}
      container
      direction="row"
      sx={{ alignItems: 'flex-start' }}
    >
      {items.map((item, index) => {
        return (
          <React.Fragment key={index}>
            {!!item && (
              // <ListItem component={Grid} item xs={12} md={6}></ListItem>
              <ListItem>
                <ListItemIcon>
                  <FontAwesomeIcon
                    icon={faCircleCheck}
                    color="#17aa1c"
                  ></FontAwesomeIcon>
                </ListItemIcon>
                <ListItemText primary={item}></ListItemText>
              </ListItem>
            )}
          </React.Fragment>
        );
      })}
    </List>
  );
};
