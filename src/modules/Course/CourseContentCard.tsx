import { Card, CardContent, CardHeader, List } from '@mui/material';
import { Fragment } from 'react';
import { BoxSpacingBottom } from '../../components/Box';
import { TypographyBorderBottom } from '../../components/Text/Typography';
import { Messages, setMessage } from '../../lib/messages';
import { CoursePartData } from '../../types/fetch-data.type';
import { CoursePartListItem } from '../CoursePart/CoursePartListItem';

type CourseContentCardProps = {
  courseParts: CoursePartData[];
};

export const CourseContentCard: React.FC<CourseContentCardProps> = ({
  courseParts,
}) => {
  return (
    <Card>
      <CardContent>
        <BoxSpacingBottom>
          <TypographyBorderBottom variant="h2">
            {setMessage(Messages.course.content)}
          </TypographyBorderBottom>
        </BoxSpacingBottom>

        <List>
          {courseParts.length > 0 &&
            courseParts.map((coursePart, index) => {
              return (
                <Fragment key={index}>
                  <CoursePartListItem number={index + 1} data={coursePart} />
                </Fragment>
              );
            })}
        </List>
      </CardContent>
    </Card>
  );
};
