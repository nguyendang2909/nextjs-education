import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Typography,
} from '@mui/material';
import { FC } from 'react';
import { Messages, setMessage } from '../../../lib/messages';
import { CourseData } from '../../../types/fetch-data.type';
import { TeacherCourseDeleteCard } from './TeacherCourseDeleteCard';

type TeacherCourseSettingPanelProps = {
  course: CourseData;
};

export const TeacherCourseSettingPanel: FC<TeacherCourseSettingPanelProps> = ({
  course,
}) => {
  return (
    <>
      <Box>
        <TeacherCourseDeleteCard course={course} />
      </Box>
    </>
  );
};
