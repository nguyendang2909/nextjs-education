import {
  faCertificate,
  faThumbsUp,
  faUsers,
} from '@fortawesome/free-solid-svg-icons';
import { Box, Grid, Rating } from '@mui/material';
import { FC } from 'react';
import { GridPaddingHorizontal } from '../../components/Grid';
import { FontAwesomeIconSpacing } from '../../components/Icon';
import { TeacherAboutGrid } from '../../components/Teacher/TeacherAboutBox';
import {
  TypographyShadowWhite,
  TypographyWhite,
} from '../../components/Text/Typography';
import { setMessage } from '../../lib/messages';
import { UserData } from '../../types/fetch-data.type';

type CourseBannerInfoBoxProps = {
  courseName?: string;
  courseSubTitle?: string;
  teacher?: UserData;
  courseAverageRatings?: number;
  courseCountRatings?: number;
  courseCountStudents?: number;
  courseCertificate?: boolean;
};

export const CourseBannerInfoBox: FC<CourseBannerInfoBoxProps> = ({
  courseName,
  teacher,
  courseSubTitle,
  courseAverageRatings,
  courseCountRatings,
  courseCountStudents,
  courseCertificate,
}) => {
  return (
    <>
      <TypographyShadowWhite variant="h1" gutterBottom>
        {setMessage(courseName)}
      </TypographyShadowWhite>

      <Box sx={{ paddingBottom: '24px' }}>
        {!!teacher && (
          <TeacherAboutGrid
            data={teacher}
            typographyProps={{ color: '#fff' }}
          />
        )}
      </Box>

      <TypographyWhite gutterBottom>{courseSubTitle}</TypographyWhite>

      <Grid
        container
        spacing={1}
        sx={{ color: '#fff', fontWeight: 600, alignItems: 'center' }}
      >
        <GridPaddingHorizontal item>
          <Grid
            container
            sx={{
              alignItems: 'center',
            }}
          >
            <FontAwesomeIconSpacing icon={faThumbsUp} />
            <Box>{courseAverageRatings}</Box>
            {!!courseAverageRatings && (
              <>
                <Rating
                  defaultValue={courseAverageRatings}
                  readOnly
                  precision={0.5}
                />
                ({courseCountRatings})
              </>
            )}
          </Grid>
        </GridPaddingHorizontal>
        <GridPaddingHorizontal item>
          <Grid
            container
            sx={{
              alignItems: 'center',
            }}
          >
            <FontAwesomeIconSpacing icon={faUsers} />
            <Box>{courseCountStudents}</Box>
          </Grid>
        </GridPaddingHorizontal>
        {courseCertificate && (
          <GridPaddingHorizontal item>
            <Grid
              container
              sx={{
                alignItems: 'center',
              }}
            >
              <FontAwesomeIconSpacing icon={faCertificate} />
              <Box>Cấp chứng nhận hoàn thành</Box>
            </Grid>
          </GridPaddingHorizontal>
        )}
      </Grid>
    </>
  );
};
