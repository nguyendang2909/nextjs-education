import {
  Grid,
  Hidden,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material';
import { FC, Fragment } from 'react';
import { BoxBackground } from '../../components/Box';
import { GridNoWrap } from '../../components/Grid';
import { TableCellReponsive } from '../../components/Table';
import { TableCellIndex } from '../../components/Table/TableCell';
import {
  TypographyCapitalize,
  TypographyTwoLine,
} from '../../components/Text/Typography';
import { Messages, setMessage } from '../../lib/messages';
import { requestAPI, requestService } from '../../lib/request';
import { CartItemData } from '../../types/fetch-data.type';
import { CartCoursePrice } from './CartPrice';

type CartItemsTableProps = {
  carts: CartItemData[];
};

export const CartItemsTable: FC<CartItemsTableProps> = ({ carts }) => {
  return (
    <>
      <Typography variant="h3" gutterBottom>
        Đơn hàng của bạn
      </Typography>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCellIndex />
              <TableCellReponsive>
                <Typography>{setMessage(Messages.course.name)}</Typography>
              </TableCellReponsive>
              <Hidden smDown>
                <TableCellReponsive sx={{ textAlign: 'center' }}>
                  <TypographyCapitalize>
                    {Messages.cart.price}
                  </TypographyCapitalize>
                </TableCellReponsive>
              </Hidden>
            </TableRow>
          </TableHead>
          <TableBody>
            {carts.map((item, index) => {
              const { course = {}, id: cartItemId } = item;

              const {
                id: courseId,
                name: courseName,
                price: coursePrice,
                promotionPrice: coursePromotionPrice,
                coverImageURL,
              } = course;

              const cartCoursePrice = (
                <CartCoursePrice
                  promotionPrice={coursePromotionPrice}
                  price={coursePrice}
                />
              );

              return (
                <Fragment key={index}>
                  {!!courseId && !!cartItemId && (
                    <TableRow>
                      <TableCell>{index + 1}</TableCell>
                      <TableCellReponsive>
                        <GridNoWrap container spacing={1}>
                          <Grid item>
                            <BoxBackground
                              sx={{
                                width: '80px',
                                height: '80px',
                                backgroundImage: coverImageURL
                                  ? `url("${requestService.getURL(
                                      coverImageURL,
                                    )}")`
                                  : undefined,
                              }}
                            />
                          </Grid>
                          <Grid item sx={{ flexGrow: 1 }}>
                            <TypographyTwoLine>
                              {setMessage(courseName)}
                            </TypographyTwoLine>
                          </Grid>
                        </GridNoWrap>
                      </TableCellReponsive>
                      <Hidden smDown>
                        <TableCellReponsive sx={{ textAlign: 'right' }}>
                          {cartCoursePrice}
                        </TableCellReponsive>
                      </Hidden>
                    </TableRow>
                  )}
                </Fragment>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};
