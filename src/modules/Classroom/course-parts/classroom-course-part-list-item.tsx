import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Collapse,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import { Fragment, useState } from 'react';
import { Messages, setMessage } from '../../../lib/messages';
import { CoursePartData } from '../../../types/fetch-data.type';
import { ClassroomLessonItem } from '../lesson/classroom-lesson-item';

type ClassroomCoursePartListItemProps = {
  selectedLessonId?: number;
  coursePart: CoursePartData;
  index: number;
  onLessonClick: (id: number) => void;
};

export const ClassroomCoursePartListItem: React.FC<
  ClassroomCoursePartListItemProps
> = props => {
  const { coursePart, index, onLessonClick, selectedLessonId } = props;

  const { name, lesson: lessons } = coursePart;

  const [isExpand, setExpand] = useState<boolean>(
    selectedLessonId
      ? !!lessons?.find(lesson => lesson?.id === selectedLessonId)
      : index === 1,
  );

  const handleSetExpand = () => {
    setExpand(!isExpand);
  };

  return (
    <>
      <ListItemButton onClick={handleSetExpand}>
        <ListItemIcon>
          {isExpand ? (
            <FontAwesomeIcon icon={faAngleDown} />
          ) : (
            <FontAwesomeIcon icon={faAngleUp} />
          )}
        </ListItemIcon>

        <ListItemText
          primaryTypographyProps={{ fontWeight: 'bold' }}
          primary={`${setMessage(Messages.course.part)} ${index}: ${setMessage(
            name,
          )}`}
        />
      </ListItemButton>

      {!!lessons && lessons.length > 0 && (
        <Collapse in={isExpand} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {lessons.map((lesson, index) => {
              const { id: lessonId, name: lessonName } = lesson;

              return (
                <Fragment key={index}>
                  {lessonId && (
                    <ClassroomLessonItem
                      selected={selectedLessonId === lessonId}
                      lessonName={lessonName}
                      onClick={() => {
                        onLessonClick(lessonId);
                      }}
                    />
                  )}
                </Fragment>
              );
            })}
          </List>
        </Collapse>
      )}
    </>
  );
};
