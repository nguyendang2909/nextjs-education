import { faFileLines } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import { Messages, setMessage } from '../../../lib/messages';
import { LessonData } from '../../../types/fetch-data.type';

type LessonListItemProps = {
  selected?: boolean;
  onClick: () => void;
  index?: number;
  lessonName?: string;
};

export const ClassroomLessonItem: React.FC<LessonListItemProps> = ({
  selected = false,
  lessonName,
  index,
  onClick,
}) => {
  return (
    <>
      <ListItemButton
        selected={selected}
        sx={{
          paddingLeft: 5,
        }}
        onClick={onClick}
      >
        <ListItemIcon>
          <FontAwesomeIcon icon={faFileLines} />
        </ListItemIcon>
        <ListItemText
          primary={`${setMessage(Messages.course.lesson)} ${
            index || ''
          }: ${setMessage(lessonName)}`}
        />
      </ListItemButton>
    </>
  );
};
