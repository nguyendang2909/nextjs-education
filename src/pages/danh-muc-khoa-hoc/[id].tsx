import {
  Breadcrumbs,
  Button,
  Container,
  Grid,
  Stack,
  Typography,
} from '@mui/material';
import { GetServerSideProps } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { Fragment } from 'react';
import { dehydrate, QueryClient, useQuery } from 'react-query';
import { Breadcrumb, BreadcrumbHome } from '../../components/Breadcrumbs';
import { LoadingContent } from '../../Layout/Content/LoadingContent';
import { courseCategoriesService } from '../../lib/course-categories.service';
import { Messages, setMessage } from '../../lib/messages';
import NextLink from 'next/link';
import { coursesService } from '../../lib/courses.service';
import _ from 'lodash';
import { NextPageWithLayout } from '../../types/components.type';
import { CoursesContainer } from '../../modules/Course/FeatureCourse';
import { BoxRightText, BoxSpacing } from '../../components/Box';

const Page: NextPageWithLayout = () => {
  const router = useRouter();

  const courseCategoryId = courseCategoriesService.getIdFromParamId(
    router.query.id as string,
  );

  const {
    isSuccess,
    data: courseCategory = {},
    isLoading,
  } = useQuery(
    ['getCourseCategory', courseCategoryId],
    () => courseCategoriesService.getFromId(courseCategoryId),
    {
      enabled: true,
      staleTime: Infinity,
    },
  );

  const fetchCoursesParams = {
    ..._.omit(router.query, 'id'),
    courseCategoryId: courseCategoryId,
  };

  const { data: courses = [] } = useQuery(
    ['getCoursesByCategory', courseCategoryId],
    () => coursesService.getMany(fetchCoursesParams),
    {
      enabled: true,
      staleTime: Infinity,
    },
  );

  const { data: countCourses = 0 } = useQuery(
    ['countCoursesByCategory', courseCategoryId],
    () => coursesService.count(fetchCoursesParams),
    { enabled: true, staleTime: Infinity },
  );

  if (isSuccess) {
    const {
      name,
      icon,
      courseSubcategory: courseSubcategories = [],
    } = courseCategory;

    return (
      <>
        <Head>
          <title>{name}</title>
        </Head>

        <Container>
          <Breadcrumbs>
            <BreadcrumbHome />
            <Breadcrumb title={name} icon={icon} />
          </Breadcrumbs>
          <Typography variant="h3" gutterBottom>
            {setMessage(name)}
          </Typography>
          <Grid sx={{ pb: 3 }}>
            <Stack direction="row" spacing={2}>
              <NextLink
                href={{
                  pathname: router.pathname,
                  query: _.omit(router.query, 'courseSubcategoryId'),
                }}
                passHref
              >
                <Button
                  variant="outlined"
                  color={
                    router.query.courseSubcategoryId === undefined
                      ? 'secondary'
                      : 'primary'
                  }
                >
                  {setMessage(Messages.common.all)}
                </Button>
              </NextLink>
              {courseSubcategories.map((courseSubcategory, index) => {
                const { id: courseSubcategoryId, name: courseSubcategoryName } =
                  courseSubcategory;

                return (
                  <Fragment key={index}>
                    {!!courseSubcategoryId && (
                      <NextLink
                        href={{
                          pathname: router.pathname,
                          query: {
                            ...router.query,
                            courseSubcategoryId: courseSubcategoryId,
                          },
                        }}
                        passHref
                      >
                        <Button
                          variant="outlined"
                          color={
                            router.query.courseSubcategoryId ===
                            courseSubcategoryId.toString()
                              ? 'secondary'
                              : 'primary'
                          }
                        >
                          {courseSubcategoryName}
                        </Button>
                      </NextLink>
                    )}
                  </Fragment>
                );
              })}
            </Stack>
          </Grid>
        </Container>

        <Container>
          <BoxSpacing>
            <BoxRightText>
              <Typography variant="subtitle1">
                {`${setMessage(Messages.action.found)} ${
                  courses.length
                }/${countCourses}`}
              </Typography>
            </BoxRightText>

            {courses.length > 0 && (
              <CoursesContainer courses={courses}></CoursesContainer>
            )}
          </BoxSpacing>
        </Container>
      </>
    );
  }

  if (isLoading) {
    return <LoadingContent />;
  }

  return <></>;
};

export const getServerSideProps: GetServerSideProps = async context => {
  const courseCategoryId = courseCategoriesService.getIdFromParamId(
    context.params?.id as string,
  );

  const queryClient = new QueryClient();

  await queryClient.prefetchQuery(['getCourseCategory', courseCategoryId], () =>
    courseCategoriesService.getFromId(courseCategoryId),
  );

  const fetchCoursesParams = {
    ..._.omit(context.query, 'id'),
    courseCategoryId,
  };

  await queryClient.prefetchQuery(
    ['getCoursesByCategory', courseCategoryId],
    () => coursesService.getMany(fetchCoursesParams),
  );

  await queryClient.prefetchQuery(
    ['countCoursesByCategory', courseCategoryId],
    () => coursesService.count(fetchCoursesParams),
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
