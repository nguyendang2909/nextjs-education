import {
  faArrowRight,
  faCertificate,
  faComments,
  faUsers,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Rating,
  Stack,
  Typography,
} from '@mui/material';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { Fragment } from 'react';
import { useQuery } from 'react-query';
import { BoxSpacingBottom } from '../../components/Box';
import { ContainerSpacing } from '../../components/Container';
import { NextLink } from '../../components/Link';
import {
  TypographyCapitalize,
  TypographyColorSecond,
} from '../../components/Text/Typography';
import { PrivateMainLayout } from '../../Layout/PrivateMainLayout';
import { coursesService } from '../../lib/courses.service';
import { Messages, messagesService, setMessage } from '../../lib/messages';
import { requestAPI, requestService } from '../../lib/request';
import { ClassroomsEmpty } from '../../modules/Classroom/ClassroomsEmpty';
import { NextPageWithLayout } from '../../types/components.type';

const Page: NextPageWithLayout = () => {
  const router = useRouter();

  const pageTitle = Messages.course.my;

  const { data: courses } = useQuery(
    'paidCourses',
    () => coursesService.getMany({ purchase: true, showCountQuestions: true }),
    {
      enabled: router.isReady,
    },
  );

  return (
    <>
      <Head>
        <title>{messagesService.setPageTitle(pageTitle)}</title>
      </Head>

      <ContainerSpacing>
        <BoxSpacingBottom>
          <Typography variant="h1">{setMessage(Messages.course.my)}</Typography>
        </BoxSpacingBottom>

        {courses ? (
          <>
            {courses.length === 0 ? (
              <></>
            ) : (
              <>
                <Stack spacing={3}>
                  {courses.map((course, index) => {
                    const {
                      id: courseId,
                      coverImageURL: courseCoverImageURL,
                      name: courseName,
                      user: courseUser,
                      countQuestions,
                      countStudents,
                      averageRatings,
                      countRatings,
                    } = course;

                    return (
                      <Fragment key={index}>
                        {courseId && (
                          <Grid item>
                            <Card>
                              <NextLink
                                href={coursesService.getLearnLink(courseId)}
                                passHref
                              >
                                <CardActionArea>
                                  <Grid container>
                                    <CardMedia
                                      component={Box}
                                      sx={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        cursor: 'pointer',
                                        width: '300px',
                                        // width: '100%',
                                        backgroundImage: courseCoverImageURL
                                          ? `url("${requestService.getURL(
                                              courseCoverImageURL,
                                            )}")`
                                          : undefined,
                                      }}
                                    ></CardMedia>
                                    <Box sx={{ flexGrow: 1 }}>
                                      <CardContent
                                        sx={{ padding: '16px 16px' }}
                                      >
                                        {/* course name */}
                                        <TypographyCapitalize variant="h3">
                                          {courseName}
                                        </TypographyCapitalize>
                                        {/* course teacher name */}
                                        <Grid container sx={{ mb: 1.5 }}>
                                          <TypographyColorSecond>
                                            {courseUser?.fullname}
                                          </TypographyColorSecond>
                                          <TypographyColorSecond>
                                            {courseUser?.title
                                              ? ` - ${courseUser.title}`
                                              : ''}
                                          </TypographyColorSecond>
                                        </Grid>
                                        {/* Course count students + count questions */}
                                        <Stack
                                          direction="row"
                                          spacing={3}
                                          sx={{ mb: 1.5 }}
                                        >
                                          <Typography>
                                            <FontAwesomeIcon icon={faUsers} />{' '}
                                            {countStudents || 0}{' '}
                                          </Typography>
                                          <Typography>
                                            <FontAwesomeIcon
                                              icon={faComments}
                                            />{' '}
                                            {countQuestions}{' '}
                                            {Messages.course.question}
                                          </Typography>
                                          {course.certificate && (
                                            <Typography>
                                              <FontAwesomeIcon
                                                icon={faCertificate}
                                              />{' '}
                                              {setMessage(
                                                Messages.course.certificate,
                                              )}
                                            </Typography>
                                          )}
                                        </Stack>
                                        {/* Course average ratings */}
                                        <Stack
                                          direction="row"
                                          alignItems="center"
                                        >
                                          {averageRatings && (
                                            <>
                                              <Rating
                                                defaultValue={averageRatings}
                                                precision={0.5}
                                                readOnly
                                              ></Rating>
                                              ({countRatings})
                                            </>
                                          )}
                                        </Stack>
                                      </CardContent>
                                      <CardActions
                                        sx={{ justifyContent: 'right' }}
                                      >
                                        <Button
                                          endIcon={
                                            <FontAwesomeIcon
                                              icon={faArrowRight}
                                            />
                                          }
                                        >
                                          {Messages.course.gotoLearn}
                                        </Button>
                                      </CardActions>
                                    </Box>
                                  </Grid>
                                </CardActionArea>
                              </NextLink>
                            </Card>
                          </Grid>
                        )}
                      </Fragment>
                    );
                  })}
                </Stack>
              </>
            )}
          </>
        ) : (
          <ContainerSpacing>
            <ClassroomsEmpty></ClassroomsEmpty>
          </ContainerSpacing>
        )}
      </ContainerSpacing>
    </>
  );
};

Page.layout = PrivateMainLayout;

export default Page;
