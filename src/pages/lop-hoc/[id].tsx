import Head from 'next/head';
import {
  Box,
  CircularProgress,
  Container,
  Grid,
  List,
  Pagination,
  Rating,
  Stack,
  Tab,
  Typography,
} from '@mui/material';
import { useRouter } from 'next/router';
import { Fragment, useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import {
  BoxCenter,
  BoxMinHeightBig,
  BoxRightText,
  BoxSpacing,
  BoxSpacingBottom,
} from '../../components/Box';
import { ClassroomCoursePartListItem } from '../../modules/Classroom/course-parts/classroom-course-part-list-item';
import { VideoPlayer } from '../../components/VideoPlayer';
import { ClassroomLayout } from '../../Layout/ClassroomLayout';
import { coursesService } from '../../lib/courses.service';
import { lessonsService } from '../../lib/lessons.service';
import { Messages, messagesService } from '../../lib/messages';
import { NextPageWithLayout } from '../../types/components.type';
import { CreateCourseRatingDialog } from '../../modules/Classroom/course-ratings/CreateCourseRating';
import { faUsers } from '@fortawesome/free-solid-svg-icons';
import { TeacherAboutGrid } from '../../components/Teacher/TeacherAboutBox';
import { TextEditorReadOnly } from '../../components/Editor/TextEditorReadOnly';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import { CreateQuestion } from '../../modules/Classroom/questions/CreateQuestion';
import { courseQuestionsService } from '../../lib/course-questions.service';
import { CourseQuestions } from '../../modules/Classroom/questions/CourseQuestions';
import { requestService } from '../../lib/request';
import { APP_API } from '../../config';
import { FontAwesomeIconSpacing } from '../../components/Icon';
import { urlQueryService } from '../../lib/url-query.service';
import { useAppSelector } from '../../store/hooks';
import { NotFoundCoursePage } from '../../components/NotFound/NotFoundCoursePage';
import { LoadingPage } from '../../components/Page/LoadingPage';
import _ from 'lodash';

enum ETab {
  about = 'about',
  document = 'document',
  myQuestions = 'myQuestions',
  questions = 'questions',
}

const Page: NextPageWithLayout = () => {
  const router = useRouter();

  const currentUserId = useAppSelector(state => state.user?.info?.id);

  const routerOptions = { router };

  const queryOptions = { query: router.query };

  const tab =
    (urlQueryService.getOne('tab', queryOptions) as ETab) || ETab.about;

  const courseId = urlQueryService.getOneAsNumber('id', queryOptions) as number;

  const questionsCurrentPage =
    urlQueryService.getOneAsNumber('questionsCurrentPage', queryOptions) || 1;

  const questionsPageSize =
    urlQueryService.getOneAsNumber('questionsPageSize', queryOptions) || 50;

  const myQuestionsCurrentPage =
    urlQueryService.getOneAsNumber('myQuestionsCurrentPage', queryOptions) || 1;

  const myQuestionsPageSize =
    urlQueryService.getOneAsNumber('myQuestionsPageSize', queryOptions) || 50;

  const [rating, setRating] = useState<number>(0);

  const {
    data: course,
    isSuccess,
    isLoading,
    isError,
  } = useQuery(
    ['learnCourse', courseId],
    () => coursesService.learnById(courseId),
    {
      enabled: router.isReady,
      staleTime: Infinity,
    },
  );

  const {
    data: courseQuestions = [],
    refetch: refetchCourseQuestions,
    isLoading: isLoadingCourseQuestions,
  } = useQuery(
    ['courseQuestions', courseId],
    () =>
      courseQuestionsService.getMany({
        courseId,
        currentPage: questionsCurrentPage,
        pageSize: questionsPageSize,
      }),
    {
      enabled: router.isReady && tab === ETab.questions,
    },
  );

  const { data: countCourseQuestions, refetch: refetchCountCourseQuestions } =
    useQuery(
      ['countCourseQuestions', courseId],
      () => courseQuestionsService.count({ courseId }),
      {
        enabled: router.isReady && tab === ETab.questions,
      },
    );

  const {
    data: myCourseQuestions = [],
    refetch: refetchMyCourseQuestions,
    isLoading: isLoadingMyCourseQuestions,
  } = useQuery(
    ['myCourseQuestions', courseId],
    () =>
      courseQuestionsService.getMany({
        courseId,
        userId: currentUserId,
        currentPage: myQuestionsCurrentPage,
        pageSize: myQuestionsPageSize,
      }),
    {
      enabled: router.isReady && tab === ETab.myQuestions,
    },
  );

  const {
    data: countMyCourseQuestions,
    refetch: refetchCountMyCourseQuestions,
  } = useQuery(
    ['countMyCourseQuestions', courseId],
    () => courseQuestionsService.count({ courseId, userId: currentUserId }),
    {
      enabled: router.isReady && tab === ETab.questions,
    },
  );

  const selectedLessonId =
    +(router.query.lessonId as string) ||
    ((course?.coursePart &&
      course.coursePart.length > 0 &&
      course.coursePart[0].lesson &&
      course.coursePart[0].lesson.length > 0 &&
      course.coursePart[0].lesson[0].id) as number);

  const { data: lesson } = useQuery(
    ['learnLesson', selectedLessonId],
    () => lessonsService.getOneById(selectedLessonId),
    {
      enabled: router.isReady && selectedLessonId > 0,
      staleTime: Infinity,
    },
  );

  useEffect(() => {
    if (router.isReady) {
      refetchCourseQuestions();
    }
  }, [
    refetchCourseQuestions,
    router.isReady,
    questionsCurrentPage,
    questionsPageSize,
  ]);

  useEffect(() => {
    if (router.isReady) {
      refetchMyCourseQuestions();
    }
  }, [
    router.isReady,
    refetchMyCourseQuestions,
    myQuestionsCurrentPage,
    myQuestionsPageSize,
  ]);

  const learnLessonById = (id: number) => {
    urlQueryService.setUrlQuery({ lessonId: id }, routerOptions);
  };

  const handleRequestCourseQuestions = () => {
    refetchCourseQuestions();

    refetchCountCourseQuestions();
  };

  const handleRequestMyCourseQuestions = () => {
    refetchMyCourseQuestions();

    refetchCountMyCourseQuestions();
  };

  const handleClickRating = (
    event: React.SyntheticEvent,
    value: number | null,
  ) => {
    value && setRating(value);
  };

  const handleChangeTab = (event: React.SyntheticEvent, newValue: ETab) => {
    urlQueryService.replaceUrlQuery({ tab: newValue }, routerOptions);
  };

  const handleChangeCourseQuestionsCurrentPage = (
    event: React.ChangeEvent<unknown>,
    page: number,
  ) => {
    urlQueryService.replaceUrlQuery(
      {
        questionsCurrentPage: page,
      },
      routerOptions,
    );
  };

  const handleChangeMyQuestionsCurrentPage = (
    event: React.ChangeEvent<unknown>,
    page: number,
  ) => {
    urlQueryService.replaceUrlQuery(
      {
        myQuestionsCurrentPage: page,
      },
      routerOptions,
    );
  };

  const questionsPagination = _.isNumber(countCourseQuestions) ? (
    <Pagination
      sx={{
        justifyContent: 'flex-end',
        display: 'fex',
      }}
      count={Math.ceil(countCourseQuestions / questionsPageSize)}
      page={questionsCurrentPage}
      onChange={handleChangeCourseQuestionsCurrentPage}
    />
  ) : (
    <></>
  );

  const myQuestionsPagination = _.isNumber(countMyCourseQuestions) ? (
    <Pagination
      sx={{
        justifyContent: 'flex-end',
        display: 'fex',
      }}
      count={Math.ceil(countMyCourseQuestions / myQuestionsPageSize)}
      page={myQuestionsCurrentPage}
      onChange={handleChangeCourseQuestionsCurrentPage}
    />
  ) : (
    <></>
  );

  const pageTitle = messagesService.setPageTitle(
    `${Messages.course.classroom} ${course?.name || ''}`,
  );

  if (isError) {
    return <NotFoundCoursePage />;
  }

  if (isLoading) {
    return <LoadingPage title={pageTitle} />;
  }

  if (isSuccess) {
    if (course) {
      return (
        <>
          <Head>
            <title>{pageTitle}</title>
          </Head>

          <>
            <Container maxWidth={false}>
              <Grid container spacing={2}>
                <Grid item xs={12} md={8}>
                  <Stack spacing={2}>
                    {/* Rate course */}
                    <Grid xs={12}>
                      <Grid container>
                        <Grid item sx={{ flexGrow: 1 }}></Grid>
                        <Grid item>
                          <Rating
                            value={
                              course.rating?.rating
                                ? course.rating.rating
                                : rating
                            }
                            onChange={handleClickRating}
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                    {/* Course content */}
                    <Grid xs={12}>
                      {lesson?.type === 'video' ? (
                        <VideoPlayer
                          url={requestService.getURL(
                            `${APP_API.lessonsVideo}/${lesson.id}`,
                          )}
                          autoPlay={true}
                        />
                      ) : (
                        <></>
                      )}
                    </Grid>
                    {/* Course info */}
                    <Grid>
                      <BoxSpacing
                        sx={{
                          borderBottom: '1px solid',
                          borderBottomColor: 'divider',
                        }}
                      >
                        <Typography variant="h1">{course.name}</Typography>
                        <BoxRightText>
                          <FontAwesomeIconSpacing icon={faUsers} />
                          {course.countStudents} {Messages.course.student}
                        </BoxRightText>
                        {!!course.user && (
                          <Box>
                            <Grid container sx={{ alignItems: 'center' }}>
                              <TeacherAboutGrid data={course.user} />
                            </Grid>
                          </Box>
                        )}
                      </BoxSpacing>
                      <TabContext value={tab}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                          <TabList onChange={handleChangeTab}>
                            <Tab
                              label={Messages.common.about}
                              value={ETab.about}
                            />
                            <Tab
                              label={Messages.course.question}
                              value={ETab.questions}
                            />
                            <Tab
                              label={Messages.course.myQuestion}
                              value={ETab.myQuestions}
                            />
                          </TabList>
                        </Box>

                        <BoxMinHeightBig>
                          <TabPanel value={ETab.about}>
                            <TextEditorReadOnly data={course.about} />
                          </TabPanel>

                          <TabPanel value={ETab.questions}>
                            <BoxSpacingBottom>
                              <CreateQuestion
                                courseId={courseId}
                                refetch={handleRequestCourseQuestions}
                              />
                            </BoxSpacingBottom>
                            {_.isNumber(countCourseQuestions) && (
                              <>
                                <BoxRightText>
                                  {countCourseQuestions}{' '}
                                  {Messages.course.question}
                                </BoxRightText>
                                <BoxSpacing>{questionsPagination}</BoxSpacing>
                              </>
                            )}
                            {isLoadingCourseQuestions ? (
                              <BoxCenter>
                                <CircularProgress />
                              </BoxCenter>
                            ) : (
                              <>
                                {!!courseQuestions &&
                                courseQuestions.length > 0 ? (
                                  <>
                                    <Box>
                                      <CourseQuestions
                                        data={courseQuestions}
                                        refetch={handleRequestCourseQuestions}
                                      />
                                    </Box>
                                  </>
                                ) : (
                                  <>
                                    <Box>Chưa có câu hỏi nào</Box>
                                  </>
                                )}
                              </>
                            )}
                            {_.isNumber(countCourseQuestions) && (
                              <>
                                <BoxSpacing>{questionsPagination}</BoxSpacing>
                              </>
                            )}
                          </TabPanel>

                          <TabPanel value={ETab.myQuestions}>
                            <BoxSpacingBottom>
                              <CreateQuestion
                                courseId={courseId}
                                refetch={handleRequestMyCourseQuestions}
                              />
                            </BoxSpacingBottom>
                            {!!countMyCourseQuestions && (
                              <>
                                <BoxRightText>
                                  {countMyCourseQuestions}{' '}
                                  {Messages.course.question}
                                </BoxRightText>
                                <BoxSpacing>{myQuestionsPagination}</BoxSpacing>
                              </>
                            )}
                            {isLoadingMyCourseQuestions ? (
                              <BoxCenter>
                                <CircularProgress />
                              </BoxCenter>
                            ) : (
                              <>
                                {!!myCourseQuestions &&
                                myCourseQuestions.length > 0 ? (
                                  <>
                                    <Box>
                                      <CourseQuestions
                                        data={myCourseQuestions}
                                        refetch={handleRequestMyCourseQuestions}
                                      />
                                    </Box>
                                  </>
                                ) : (
                                  <>
                                    <Box>Bạn chưa có câu hỏi nào</Box>
                                  </>
                                )}
                              </>
                            )}
                            {!!countMyCourseQuestions && (
                              <>
                                <BoxSpacing>{myQuestionsPagination}</BoxSpacing>
                              </>
                            )}
                          </TabPanel>
                        </BoxMinHeightBig>
                      </TabContext>
                    </Grid>
                  </Stack>
                </Grid>

                <Grid item xs={12} md={4}>
                  <Stack spacing={2}>
                    <Box>Nội dung học</Box>
                    <Box>
                      <List>
                        {course.coursePart?.map((coursePart, index) => {
                          return (
                            <ClassroomCoursePartListItem
                              coursePart={coursePart}
                              index={index + 1}
                              key={index}
                              onLessonClick={learnLessonById}
                              selectedLessonId={selectedLessonId}
                            ></ClassroomCoursePartListItem>
                          );
                        })}
                      </List>
                    </Box>
                  </Stack>
                </Grid>
              </Grid>
            </Container>
            <Container></Container>

            {rating > 0 && !course.rating?.rating && (
              <CreateCourseRatingDialog
                courseId={courseId}
                rating={rating}
                onClose={() => {
                  setRating(0);
                }}
              />
            )}
          </>
        </>
      );
    }

    return (
      <>
        <NotFoundCoursePage />;
      </>
    );
  }

  return <></>;
};

Page.layout = ClassroomLayout;

export default Page;
