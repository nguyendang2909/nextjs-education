import { PrivateMainLayout } from '../../Layout/PrivateMainLayout';
import { NextPageWithLayout } from '../../types/components.type';

const Page: NextPageWithLayout = () => {
  return <></>;
};

Page.layout = PrivateMainLayout;

export default Page;
