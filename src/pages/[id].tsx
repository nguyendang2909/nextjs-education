import { Grid, Hidden, Stack, Container } from '@mui/material';
import { Box } from '@mui/system';
import Head from 'next/head';
import { CourseBreadcrumbs } from '../components/Breadcrumbs/CourseBreadcrumbs';
import { CourseAboutCard } from '../modules/Course/CourseAbout';
import { CourseContentCard } from '../modules/Course/CourseContentCard';
import { CourseIntroduction } from '../modules/Course/CourseIntroductionCard';
import { CourseLearnCard } from '../modules/Course/CourseLearnCard';
import { CourseOutputCard } from '../modules/Course/CourseOutput';
import { coursesService } from '../lib/courses.service';
import { Messages, messagesService } from '../lib/messages';
import { dehydrate, QueryClient, useQuery } from 'react-query';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';
import { NotFoundCoursePage } from '../components/NotFound/NotFoundCoursePage';
import { NextPageWithLayout } from '../types/components.type';
import { ContainerSpacing } from '../components/Container';
import { requestAPI, requestService } from '../lib/request';
import { APP_API, APP_URL } from '../config';
import { courseRatingsService } from '../lib/course-ratings.service';
import { TypographyCapitalize } from '../components/Text/Typography';
import { BoxBackground } from '../components/Box';
import { Fragment } from 'react';
import { CourseRatingsStack } from '../modules/CourseRating/CourseRatingBoxes';
import { CourseRatingAverageGridContainer } from '../modules/CourseRating/CourseRatingAverageOveview';
import { CourseInfoMenuCard } from '../modules/Course/CourseInfoMenuCard';
import { CourseBannerInfoBox } from '../modules/Course/CourseBannerInfoBox';
import { LoadingPage } from '../components/Page/LoadingPage';
import { NotFoundPage } from '../components/NotFound/NotFountPage';
import { useAppSelector } from '../store/hooks';
import { cartsService } from '../lib/carts.service';
import { notificationService } from '../lib/notificationService';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { fetchCartCountThunk } from '../store/reducers/cart.reducer';
import { CourseLearnNav } from '../modules/Course/CourseLearnNav';

const Page: NextPageWithLayout = props => {
  const router = useRouter();

  const currentUrl = router.asPath;

  const courseId = coursesService.getIdFromParamId(router.query.id as string);

  const currentUserId = useAppSelector(state => state.user?.info?.id);

  const dispatch = useDispatch();

  const {
    isSuccess,
    data: course,
    isLoading,
    isError,
    refetch,
  } = useQuery(
    ['course', courseId],
    () => coursesService.getOneById(courseId),
    {
      enabled: router.isReady,
      staleTime: Infinity,
    },
  );

  const { data: courseRatings } = useQuery(
    ['courseRating', courseId],
    () => courseRatingsService.getMany({ courseId: courseId }),
    {
      enabled: router.isReady,
      staleTime: Infinity,
    },
  );

  const { data: countCourseRatings } = useQuery(
    ['countCourseRatings', courseId],
    () => courseRatingsService.count({ courseId: courseId }),
    {
      enabled: router.isReady,
      staleTime: Infinity,
    },
  );

  const pageTitle = messagesService.setPageTitle(
    course?.name || Messages.course.name,
  );

  const registerToLearn = async () => {
    if (!currentUserId) {
      router.push({
        pathname: APP_URL.login,
        query: {
          redirect: currentUrl,
        },
      });

      return;
    }

    try {
      const registerCart = await cartsService.addToCart({ courseId });

      if (registerCart) {
        if (course?.promotionPrice === 0 || course?.price === 0) {
          refetch();
        } else {
          router.push({
            pathname: APP_URL.cart,
            query: {
              cartId: registerCart.id,
            },
          });
        }
      }
    } catch (err) {
      notificationService.handleError(err);
    }
  };

  const addToCart = async () => {
    if (!currentUserId) {
      router.push({
        pathname: APP_URL.login,
        query: {
          redirect: currentUrl,
        },
      });

      return;
    }

    try {
      await cartsService.addToCart({ courseId });

      dispatch(fetchCartCountThunk());

      toast.success('Thêm vào giỏ thành công');
    } catch (err: unknown) {
      if (err instanceof Error) {
        toast.error(err.message);
      }
    }
  };

  if (isError) {
    if (courseId) {
      return <NotFoundCoursePage />;
    } else {
      return <NotFoundPage />;
    }
  }

  if (isLoading) {
    return <LoadingPage title={pageTitle} />;
  }

  if (isSuccess && course) {
    return (
      <>
        <Head>
          <title>{pageTitle}</title>
        </Head>

        <Hidden lgDown>
          <Box
            sx={{
              position: 'fixed',
              top: '120px',
              left: 'calc((100% - 1200px) /2 + 816px)',
            }}
          >
            <CourseLearnCard
              courseId={courseId}
              refetch={refetch}
              countLessons={course.countLessons}
              countStudents={course.countStudents}
              duration={course.duration}
              price={course.price}
              promotionPrice={course.promotionPrice}
              paid={course.purchase?.paid}
              updatedAt={course.updatedAt}
              coverImageURL={course.coverImageURL}
              onClickAddToCart={addToCart}
              onClickRegisterToLearn={registerToLearn}
            />
          </Box>
        </Hidden>

        <BoxBackground
          sx={{
            marginTop: '-16px',
            backgroundImage: course.bannerURL
              ? `linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url("${requestService.getURL(
                  course.bannerURL,
                )}")`
              : 'linear-gradient( #0b3955, rgba(0, 0, 0, 0.5) )',
            backgroundBlendMode: 'darken',
          }}
        >
          <Container>
            <Box
              sx={{
                maxWidth: '768px',
                paddingTop: '24px',
                paddingBottom: '24px',
              }}
            >
              <CourseBreadcrumbs
                sx={{ color: '#fff' }}
                data={course.courseSubcategory}
              />
              <CourseBannerInfoBox
                courseName={course.name}
                teacher={course.user}
                courseSubTitle={course.subTitle}
                courseAverageRatings={countCourseRatings?.average}
                courseCountRatings={course.countRatings}
                courseCountStudents={course.countStudents}
                courseCertificate={course.certificate}
              />
            </Box>
          </Container>
        </BoxBackground>

        <ContainerSpacing>
          <Box sx={{ maxWidth: '768px' }}>
            <Stack spacing={4}>
              {!!course.introductionVideoURL && (
                <Grid item xs={12}>
                  <CourseIntroduction
                    courseId={courseId}
                    introductionVideoURL={course.introductionVideoURL}
                    coverImageURL={course.coverImageURL}
                  />
                </Grid>
              )}

              <Hidden lgUp>
                <CourseLearnNav
                  paid={!!course.purchase?.paid}
                  price={course.price}
                  promotionPrice={course.promotionPrice}
                  onClickAddToCart={addToCart}
                  onClickRegisterToLearn={registerToLearn}
                />
              </Hidden>

              <CourseInfoMenuCard />

              {!!course.output && (
                <Grid
                  item
                  id="course-output"
                  sx={{
                    scrollMarginTop: '100px',
                  }}
                >
                  <CourseOutputCard courseOutput={course.output} />
                </Grid>
              )}

              {!!course.about && (
                <Grid
                  item
                  id="course-about"
                  sx={{
                    scrollMarginTop: '100px',
                  }}
                >
                  <CourseAboutCard courseAbout={course.about} />
                </Grid>
              )}

              {!!course.coursePart && (
                <Grid
                  item
                  id="course-content"
                  sx={{
                    scrollMarginTop: '100px',
                  }}
                >
                  <CourseContentCard courseParts={course.coursePart} />
                </Grid>
              )}

              <Grid
                item
                id="course-review"
                sx={{
                  scrollMarginTop: '100px',
                }}
              >
                <TypographyCapitalize variant="h2">
                  Đánh giá từ học viên
                </TypographyCapitalize>
                {countCourseRatings?.average ? (
                  <>
                    <CourseRatingAverageGridContainer
                      countCourseRatings={countCourseRatings}
                    />

                    {!!courseRatings && (
                      <CourseRatingsStack courseRatings={courseRatings} />
                    )}
                  </>
                ) : (
                  <>
                    <></>
                  </>
                )}
              </Grid>
            </Stack>
          </Box>
        </ContainerSpacing>
      </>
    );
  }

  return <></>;
};

export const getServerSideProps: GetServerSideProps = async context => {
  const queryClient = new QueryClient();

  const courseId = coursesService.getIdFromParamId(
    context.params?.id as string,
  );

  if (!courseId) {
    return { notFound: true };
  }

  const getCourse = async () => {
    return await requestAPI.get(`${APP_API.courses}/${courseId}`, {
      params: { checkPurchased: true },
      headers: {
        Cookie: context.req.headers.cookie || '',
      },
    });
  };

  await queryClient.prefetchQuery(['course', courseId], () => getCourse());

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
