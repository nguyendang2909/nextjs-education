import NextLink from 'next/link';
import Head from 'next/head';
import { Button, Container, Typography, useTheme } from '@mui/material';
import { Box } from '@mui/system';
import { GetServerSideProps } from 'next';
import { Fragment } from 'react';
import { dehydrate, QueryClient, useQuery } from 'react-query';
import { CourseCategoriesHorizontalMenu } from '../components/Menu/CourseCategoriesHorizontalMenu';
import { MainLayout } from '../Layout/MainLayout';
import { courseCategoriesService } from '../lib/course-categories.service';
import { coursesService } from '../lib/courses.service';
import { appShortTitle, Messages, setMessage } from '../lib/messages';
import { teachersService } from '../lib/teachers.service';
import { NextPageWithLayout } from '../types/components.type';
import { ContainerSpacing, ContainerSpacingBig } from '../components/Container';
import { BoxBackground, BoxSpacing, BoxSpacingBottom } from '../components/Box';
import { TypographyWhite } from '../components/Text/Typography';
import { CoursesSlider } from '../modules/Course/CourseSlider';
import { TeachersSlider } from '../modules/Teacher/TeachersSlider';
import { AppBannerSlider } from '../modules/Banner/AppBannerSlider';
import { CourseStarter } from '../modules/Index/CourseStarter';
import { ReasonToStudyBox } from '../components/Box/ReasonToStudyBox';
import { CourseCategoryGridCards } from '../modules/CourseCategory/CourseCategoryGridCards';

const Page: NextPageWithLayout = props => {
  const theme = useTheme();

  const { data: courseCategories } = useQuery(
    'courseCategories',
    () => courseCategoriesService.getAll({ countCourses: true }),
    {
      enabled: true,
      staleTime: Infinity,
    },
  );

  const { data: featureTeachers = [] } = useQuery(
    'getFeatureTeachers',
    () => teachersService.getMany({ feature: true }),
    {
      enabled: true,
      staleTime: Infinity,
    },
  );

  const { data: topSaleCourses } = useQuery('topSaleCourses', () =>
    coursesService.getMany({ sortBy: 'popularity', pageSize: 20 }),
  );

  const { data: freeCourses = [] } = useQuery('freeCourses', () =>
    coursesService.getMany({ free: true, pageSize: 20 }),
  );

  return (
    <>
      <Head>
        <title>{setMessage(Messages.app.title)}</title>
      </Head>

      {!!courseCategories && courseCategories.length > 0 && (
        <Container>
          <CourseCategoriesHorizontalMenu data={courseCategories} />
        </Container>
      )}

      <BoxSpacing>
        <Container>
          <AppBannerSlider />
        </Container>
      </BoxSpacing>

      <ContainerSpacingBig>
        <CourseStarter />
      </ContainerSpacingBig>

      {!!topSaleCourses && topSaleCourses.length > 0 && (
        <ContainerSpacing>
          <Typography variant="h2">
            {setMessage(Messages.course.topSale)}
          </Typography>
          <Box>
            <CoursesSlider courses={topSaleCourses} />
          </Box>
        </ContainerSpacing>
      )}

      {freeCourses?.length > 0 && (
        <>
          <ContainerSpacingBig>
            <Typography variant="h2">
              {setMessage(Messages.course.name, Messages.course.free)}
            </Typography>
            <Box>
              <CoursesSlider courses={freeCourses} />
            </Box>
          </ContainerSpacingBig>
        </>
      )}

      <ContainerSpacingBig>
        <BoxSpacingBottom>
          <Typography
            variant="h2"
            sx={{ textAlign: 'center', padding: theme.spacing(1, 0) }}
          >
            Chưa tìm thấy khoá học?
          </Typography>
          <Typography
            variant="subtitle1"
            sx={{ textAlign: 'center', padding: theme.spacing(1, 0) }}
          >
            Hơn 2.000 khoá học đang chờ bạn khám phá
          </Typography>
        </BoxSpacingBottom>

        {!!courseCategories && courseCategories.length > 0 && (
          <Box sx={{ padding: theme.spacing(1, 0) }}>
            <CourseCategoryGridCards courseCategories={courseCategories} />
          </Box>
        )}
      </ContainerSpacingBig>

      <ContainerSpacingBig>
        <ReasonToStudyBox />
      </ContainerSpacingBig>

      <ContainerSpacingBig>
        <BoxSpacingBottom>
          <Typography
            variant="h2"
            sx={{ textAlign: 'center', textTransform: 'uppercase' }}
          >
            Giảng viên tiêu biểu
          </Typography>
        </BoxSpacingBottom>
        <BoxSpacing>
          <TeachersSlider teachers={featureTeachers} />
        </BoxSpacing>
      </ContainerSpacingBig>

      {/* Dang ky giang vien */}
      <BoxBackground
        sx={{
          backgroundImage:
            'url("/static/images/backgrounds/dang-ky-giang-vien.jpg")',
        }}
      >
        <ContainerSpacingBig>
          <TypographyWhite
            variant="h2"
            sx={{ textAlign: 'center', padding: theme.spacing(2.5, 0) }}
          >
            Trở thành Giảng viên {appShortTitle}
          </TypographyWhite>
          <TypographyWhite
            sx={{ textAlign: 'center', padding: theme.spacing(2.5, 0) }}
          >
            Hơn 500 giảng viên đã có khóa học trên {appShortTitle}
          </TypographyWhite>
          <Box sx={{ textAlign: 'center', padding: theme.spacing(2.5, 0) }}>
            <NextLink href="/dang-ky-giang-vien" passHref>
              <Button
                size="large"
                variant="outlined"
                sx={{
                  color: '#fff',
                  borderColor: '#fff',
                  textTransform: 'uppercase',
                }}
              >
                {Messages.action.registerNow}
              </Button>
            </NextLink>
          </Box>
        </ContainerSpacingBig>
      </BoxBackground>
    </>
  );
};

Page.layout = MainLayout;

export const getServerSideProps: GetServerSideProps = async context => {
  const queryClient = new QueryClient();

  // await queryClient.prefetchQuery('getCourses', () => coursesService.getMany());

  await queryClient.prefetchQuery('courseCategories', () =>
    courseCategoriesService.getAll({ countCourses: true }),
  );

  await queryClient.prefetchQuery('getFeatureTeachers', () =>
    teachersService.getMany({ feature: true }),
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Page;
