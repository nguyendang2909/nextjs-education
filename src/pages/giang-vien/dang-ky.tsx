import { LoadingButton } from '@mui/lab';
import { Box, Button, Grid } from '@mui/material';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { ContainerSpacing } from '../../components/Container';
import { NextLink } from '../../components/Link';
import { AppPage } from '../../components/Page/AppPage';
import { TypographyCenter } from '../../components/Text/Typography';
import { CardEditUserInfo } from '../../components/User/CardEditUserInfo';
import { RegisterTeacherPending } from '../../components/User/RegisterTeacher/RegisterTeacherPending';
import { APP_URL } from '../../config';
import { PrivateMainLayout } from '../../Layout/PrivateMainLayout';
import { appShortTitle, Messages } from '../../lib/messages';
import { notificationService } from '../../lib/notificationService';
import { usersService } from '../../lib/users.service';
import { useAppSelector } from '../../store/hooks';
import { fetchCurrentUserThunk } from '../../store/reducers/user.reducer';
import { NextPageWithLayout } from '../../types/components.type';
import { ERegisterTeacher, ERole } from '../../types/enums';

const Page: NextPageWithLayout = () => {
  const userInfo = useAppSelector(state => state.user?.info);

  const [isLoading, setLoading] = useState<boolean>(false);

  const dispatch = useDispatch();

  const handleRegisterTeacher = async (register: ERegisterTeacher) => {
    try {
      setLoading(true);

      await usersService.update({ registerTeacher: register });

      dispatch(fetchCurrentUserThunk());
    } catch (err) {
      notificationService.handleError(err);
    } finally {
      setLoading(false);
    }
  };

  return (
    <AppPage title={`${Messages.action.register} ${Messages.teacher.name}`}>
      <ContainerSpacing>
        <TypographyCenter variant="h1">ĐĂNG KÝ GIẢNG VIÊN</TypographyCenter>
      </ContainerSpacing>

      <ContainerSpacing>
        {userInfo?.role === ERole.Teacher && (
          <>
            <TypographyCenter variant="h3" gutterBottom>
              Bạn đã là giảng viên
            </TypographyCenter>
            <Box sx={{ textAlign: 'center' }}>
              <NextLink href={APP_URL.teacher.management}>
                <Button size="large" variant="contained">
                  Truy cập trang quản lý
                </Button>
              </NextLink>
            </Box>
          </>
        )}

        {userInfo?.role !== ERole.Teacher &&
          userInfo?.registerTeacher === ERegisterTeacher.pending && (
            <RegisterTeacherPending />
          )}

        {userInfo?.role !== ERole.Teacher &&
          userInfo?.registerTeacher === ERegisterTeacher.pending &&
          (!userInfo?.fullname ||
            !userInfo?.avatarURL ||
            !userInfo?.description ||
            !userInfo.title) && (
            <>
              <TypographyCenter gutterBottom>
                Bạn cần bổ sung thông tin cá nhân trước khi đăng ký giảng viên
              </TypographyCenter>

              <Grid item xs={12} md={6} sx={{ margin: '0 auto' }}>
                <CardEditUserInfo
                  fullname
                  avatar
                  title
                  address
                  experience
                  description
                />
              </Grid>
            </>
          )}

        {userInfo?.role !== ERole.Teacher &&
          userInfo?.registerTeacher !== ERegisterTeacher.pending &&
          !!userInfo?.fullname &&
          !!userInfo?.avatarURL &&
          !!userInfo?.description &&
          !!userInfo.title && (
            <>
              <>
                <TypographyCenter variant="h4" gutterBottom>
                  Bạn đã điền đầy thủ thông tin để đăng ký giảng viên với{' '}
                  {appShortTitle}.
                </TypographyCenter>
                <TypographyCenter variant="h4" gutterBottom>
                  Vui lòng nhấn xác nhận để đăng ký.
                </TypographyCenter>
                <Box sx={{ textAlign: 'center' }}>
                  <LoadingButton
                    variant="contained"
                    size="large"
                    loading={isLoading}
                    onClick={() => {
                      handleRegisterTeacher(ERegisterTeacher.pending);
                    }}
                    sx={{ width: 200 }}
                  >
                    {Messages.action.register} {Messages.teacher.name}
                  </LoadingButton>
                </Box>
              </>
            </>
          )}
      </ContainerSpacing>
    </AppPage>
  );
};

Page.layout = PrivateMainLayout;

export default Page;
