import {
  faEdit,
  faPlus,
  faSort,
  faTrash,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Table,
  Box,
  Button,
  Container,
  Stack,
  TableContainer,
  Typography,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Link,
  TableFooter,
  TablePagination,
} from '@mui/material';
import Head from 'next/head';
import { NextLink } from '../../../components/Link';
import { APP_URL } from '../../../config';
import { TeacherLayout } from '../../../Layout/TeacherLayout';
import {
  Messages,
  messagesService,
  setMessage,
  setSuccessMessage,
} from '../../../lib/messages';
import { NextPageWithLayout } from '../../../types/components.type';
import { StackSpaceBetween } from '../../../components/Stack';
import { IconButtonMedium } from '../../../components/Button/IconButton';
import { MoreMenu } from '../../../components/Menu/MoreMenu';
import { coursePublishStatus, ECoursePublish } from '../../../types/enums';
import { Fragment, useEffect, useState } from 'react';
import { DeleteDialog } from '../../../components/Dialog/DeleteDialog';
import { useRouter } from 'next/router';
import { notificationService } from '../../../lib/notificationService';
import { urlQueryService } from '../../../lib/url-query.service';
import { useQuery } from 'react-query';
import { teacherCoursesService } from '../../../lib/teacher-courses.service';
import { CourseData } from '../../../types/fetch-data.type';
import { toast } from 'react-toastify';
import { SortDirection, TableSort } from '../../../components/Table/TableSort';
import {
  TFindAllCoursesDto,
  TFindManyCoursesDto,
} from '../../../types/request.dto';
import { TablePopoverFilter } from '../../../components/Table/TablePopoverFilter';
import { Formatter } from '../../../lib/formatter';
import _ from 'lodash';
import { LoadingContainer } from '../../../components/Container/LoadingContainer';
import { TableCellIndex } from '../../../components/Table/TableCell';
import { TableHeaderCellCreatedAt } from '../../../modules/Record/TableCellCreatedAt';
import { ContainerSpacing } from '../../../components/Container';
import { EditButton } from '../../../components/Button/ActionButton';
import { ButtonLink } from '../../../components/Button/ButtonLink';

const Page: NextPageWithLayout = () => {
  const router = useRouter();

  const routerOptions = { router };

  const queryOptions = { query: router.query };

  const { currentPage, pageSize } = urlQueryService.getPaginated(queryOptions);

  const publish = urlQueryService.getOne(
    'publish',
    queryOptions,
  ) as ECoursePublish;

  const sortName = urlQueryService.getOne(
    'sortName',
    queryOptions,
  ) as SortDirection;

  const sortCreatedAt = urlQueryService.getOne(
    'sortCreatedAt',
    queryOptions,
  ) as SortDirection;

  const findAllOptions: TFindAllCoursesDto = {
    publish,
  };

  const findManyOptions: TFindManyCoursesDto = {
    ...findAllOptions,
    pageSize,
    currentPage,
    sortName,
    sortCreatedAt,
  };

  const {
    data: courses,
    refetch: refetchCourses,
    isSuccess,
    isError,
    isFetching,
    isLoading,
  } = useQuery('teacherCourses', () =>
    teacherCoursesService.getMany(findManyOptions),
  );

  const { data: countCourses, refetch: refetchCountCourses } = useQuery(
    'teacherCountCourses',
    () => teacherCoursesService.count(findAllOptions),
  );

  const [deleteCourse, setDeleteCourse] = useState<CourseData>({});

  useEffect(() => {
    if (router.isReady) {
      refetchCourses();
    }
  }, [
    refetchCourses,
    router.isReady,
    pageSize,
    currentPage,
    sortName,
    sortCreatedAt,
    publish,
  ]);

  useEffect(() => {
    if (router.isReady) {
      refetchCountCourses();
    }
  }, [router.isReady, refetchCountCourses, publish]);

  const handleClickDeleteCourse = async (courseId: number) => {
    try {
      const data = await teacherCoursesService.getOneById(courseId);

      if (data) {
        setDeleteCourse(data);
      }
    } catch (err) {
      setDeleteCourse({});

      notificationService.handleError(err);
    }
  };

  const handleDeleteCourse = async () => {
    try {
      if (!deleteCourse.id) {
        return;
      }

      await teacherCoursesService.delete(deleteCourse.id);

      toast.success(
        setSuccessMessage(Messages.action.delete, Messages.course.name),
      );
    } catch (err) {
      if (err instanceof Error) {
        toast.error(err.message);
      }
    }
  };

  const pagination = (
    <TablePagination
      page={currentPage - 1}
      count={_.isNumber(countCourses) ? countCourses : 0}
      rowsPerPage={pageSize}
      onPageChange={(event, page) => {
        urlQueryService.replaceUrlQuery(
          {
            currentPage: page + 1,
          },
          routerOptions,
        );
      }}
      onRowsPerPageChange={evt => {
        urlQueryService.replaceUrlQuery(
          { pageSize: evt.target.value },
          routerOptions,
        );
      }}
    ></TablePagination>
  );

  return (
    <>
      <Head>
        <title>
          {messagesService.setPageTitle(
            `${Messages.action.manage} ${Messages.course.name}`,
          )}
        </title>
      </Head>

      <Container>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          mb={4}
        >
          <Typography variant="h1" gutterBottom>
            {setMessage(Messages.course.own)}
          </Typography>

          <NextLink href={APP_URL.teacher.createCourse} passHref>
            <Button
              size="large"
              variant="contained"
              startIcon={<FontAwesomeIcon icon={faPlus} />}
            >
              {setMessage(`${Messages.course.own} ${Messages.common.new}`)}
            </Button>
          </NextLink>
        </Stack>
      </Container>

      {isLoading && <LoadingContainer />}

      {isSuccess && (
        <ContainerSpacing>
          <TableContainer>
            {isFetching && <LoadingContainer />}

            <Table>
              <TableHead>
                <TableRow>{pagination}</TableRow>
                <TableRow>
                  <TableCellIndex />
                  <TableCell>
                    <StackSpaceBetween>
                      <Box>{setMessage(Messages.course.name)}</Box>
                      <Box>
                        <TableSort urlQueryName="sortName" />
                      </Box>
                    </StackSpaceBetween>
                  </TableCell>
                  <TableCell sx={{ width: 200 }}>
                    <StackSpaceBetween>
                      <Box>{setMessage(Messages.courseCategory.name)}</Box>
                    </StackSpaceBetween>
                  </TableCell>
                  <TableCell sx={{ width: 100 }}>
                    <StackSpaceBetween>
                      <Box>{setMessage(Messages.course.price)}</Box>
                    </StackSpaceBetween>
                  </TableCell>
                  <TableCell sx={{ width: 100 }}>
                    <StackSpaceBetween>
                      <Box>Giá giảm</Box>
                    </StackSpaceBetween>
                  </TableCell>
                  <TableCell sx={{ width: 180 }}>
                    <StackSpaceBetween>
                      <Box>{setMessage(Messages.course.publishStatus)}</Box>
                      <Box>
                        <TablePopoverFilter
                          urlQueryName="publish"
                          options={[
                            {
                              text: Formatter.formatCoursePublish(
                                ECoursePublish.Published,
                              ),
                              value: ECoursePublish.Published,
                            },
                            {
                              text: Formatter.formatCoursePublish(
                                ECoursePublish.Pending,
                              ),
                              value: ECoursePublish.Pending,
                            },
                            {
                              text: Formatter.formatCoursePublish(
                                ECoursePublish.NotPublished,
                              ),
                              value: ECoursePublish.NotPublished,
                            },
                            {
                              text: Formatter.formatCoursePublish(
                                ECoursePublish.Rejected,
                              ),
                              value: ECoursePublish.Rejected,
                            },
                          ]}
                        />
                      </Box>
                    </StackSpaceBetween>
                  </TableCell>
                  <TableHeaderCellCreatedAt />
                  <TableCell sx={{ width: 30 }}></TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {!!courses && courses?.length > 0 && (
                  <>
                    {courses?.map((course, index) => {
                      const {
                        id: courseId,
                        name: courseName,
                        courseSubcategory,
                        price,
                        promotionPrice,
                        publish,
                      } = course;

                      const courseSubcategoryId = courseSubcategory?.id;

                      const courseSubcategoryName = courseSubcategory?.name;

                      const courseCategoryName =
                        courseSubcategory?.courseCategory?.name;

                      const courseCategoryIcon =
                        courseSubcategory?.courseCategory?.icon;

                      const courseCategoryId =
                        courseSubcategory?.courseCategory?.id;

                      const rowId = pageSize * (currentPage - 1) + index + 1;

                      return (
                        <Fragment key={index}>
                          {courseId && (
                            <TableRow>
                              <TableCell>{rowId}</TableCell>
                              <TableCell>
                                <NextLink
                                  href={`${APP_URL.teacher.courses}/${courseId}`}
                                  passHref
                                >
                                  <Link>{courseName}</Link>
                                </NextLink>
                              </TableCell>
                              <TableCell>
                                {!!courseCategoryId && (
                                  <>
                                    <FontAwesomeIcon
                                      icon={courseCategoryIcon || 'question'}
                                    />{' '}
                                    {courseCategoryName} \{' '}
                                    {courseSubcategoryId &&
                                      courseSubcategoryName}
                                  </>
                                )}
                              </TableCell>
                              <TableCell>{price}</TableCell>
                              <TableCell>{promotionPrice}</TableCell>
                              <TableCell>
                                {publish && (
                                  <>
                                    {setMessage(coursePublishStatus[publish])}{' '}
                                  </>
                                )}
                              </TableCell>
                              <TableCell>
                                {!!course.createdAt &&
                                  Formatter.formatTime(course.createdAt)}
                              </TableCell>
                              <TableCell>
                                <ButtonLink
                                  href={`${APP_URL.teacher.courses}/${courseId}`}
                                >
                                  {Messages.action.edit}
                                </ButtonLink>
                              </TableCell>
                            </TableRow>
                          )}
                        </Fragment>
                      );
                    })}
                  </>
                )}
              </TableBody>
              <TableFooter>
                <TableRow>{pagination}</TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </ContainerSpacing>
      )}

      <DeleteDialog
        name={`${Messages.course.name} ${setMessage(deleteCourse.name)}`}
        open={!!deleteCourse.id}
        onClose={() => {
          setDeleteCourse({});
        }}
        onDelete={handleDeleteCourse}
        onFinish={refetchCourses}
      />
    </>
  );
};

Page.layout = TeacherLayout;

export default Page;
