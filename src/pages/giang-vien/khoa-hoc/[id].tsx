import Head from 'next/head';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import {
  Alert,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Container,
  Grid,
  Tab,
  Typography,
} from '@mui/material';
import { useRouter } from 'next/router';
import { useQuery } from 'react-query';
import { ContainerSpacing } from '../../../components/Container';
import { CourseContentEditCard } from '../../../modules/Course/Edit/CourseContent';
import { CourseAboutEditCard } from '../../../modules/Course/Edit/EditCourseAbout';
import { CourseBannerEditCard } from '../../../modules/Course/Edit/EditCourseBanner';
import { CourseCoverImageEditCard } from '../../../modules/Course/Edit/EditCourseCoverImage';
import { CourseIntroductionEditCard } from '../../../modules/Course/Edit/EditCourseIntroduction';
import { CourseOutputEditCard } from '../../../modules/Course/Edit/EditCourseOutput';
import { CoursePriceEditCard } from '../../../modules/Course/Edit/EditCoursePrice';
import { CourseTitleEditCard } from '../../../modules/Course/Edit/EditCourseTitle';
import { TeacherLayout } from '../../../Layout/TeacherLayout';
import { Messages, messagesService, setMessage } from '../../../lib/messages';
import { teacherCoursesService } from '../../../lib/teacher-courses.service';
import { NextPageWithLayout } from '../../../types/components.type';
import { urlQueryService } from '../../../lib/url-query.service';
import { NotFoundContainer } from '../../../components/NotFound/NotFoundContainer';
import { LoadingContainer } from '../../../components/Container/LoadingContainer';
import { TeacherCourseSettingPanel } from '../../../modules/Course/Teacher/TeacherCourseSettingPanel';

enum ETab {
  generelInformation = 'generel_information',
  content = 'content',
  about = 'about',
  questions = 'questions',
  setting = 'setting',
}

const Page: NextPageWithLayout = () => {
  const router = useRouter();

  const routerOptions = { router };

  const queryOptions = { query: router.query };

  const id = urlQueryService.getOneAsNumber('id', queryOptions) as number;

  const tab = (urlQueryService.getOne('tab', queryOptions) ||
    ETab.generelInformation) as ETab;

  const setTab = (value: ETab) => {
    urlQueryService.setUrlQuery({ tab: value }, routerOptions);
  };

  const {
    data: course,
    refetch,
    isError,
    isSuccess,
    isLoading,
    isFetching,
  } = useQuery(
    ['teacherCourse', id],
    () => teacherCoursesService.getOneById(id),
    {
      enabled: router.isReady,
      staleTime: Infinity,
    },
  );

  const pageTitle = `${Messages.course.name} "${
    course?.name ? setMessage(course.name) : ''
  }"`;

  const handleChangeTab = (event: React.SyntheticEvent, newValue: ETab) => {
    setTab(newValue);
  };

  return (
    <>
      <Head>
        <title>{messagesService.setPageTitle(pageTitle)}</title>
      </Head>

      {isError && <NotFoundContainer />}

      {isLoading && <LoadingContainer />}

      {isSuccess && !!course && !!course.id && (
        <>
          <ContainerSpacing>
            <Box sx={{ paddingBottom: 3 }}>
              <Typography variant="h4" gutterBottom>
                {setMessage(pageTitle)}
              </Typography>
            </Box>
          </ContainerSpacing>

          <TabContext value={tab}>
            <ContainerSpacing maxWidth={false}>
              <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <TabList onChange={handleChangeTab}>
                  <Tab
                    label={Messages.common.generalInformation}
                    value={ETab.generelInformation}
                  />
                  <Tab label={Messages.common.about} value={ETab.about} />
                  <Tab label={Messages.course.content} value={ETab.content} />
                  <Tab
                    label={Messages.course.question}
                    value={ETab.questions}
                  />
                  <Tab label={Messages.common.setting} value={ETab.setting} />
                </TabList>
              </Box>
            </ContainerSpacing>

            <TabPanel value={ETab.generelInformation}>
              <ContainerSpacing>
                <Grid container direction="column" spacing={2}>
                  <Grid item>
                    <Grid container spacing={2} sx={{ alignItems: 'stretch' }}>
                      <Grid item>
                        <CourseCoverImageEditCard
                          onFinish={refetch}
                          courseId={course.id}
                          coverImageURL={course.coverImageURL}
                          sx={{ height: '100%' }}
                        />
                      </Grid>
                      <Grid item flex="1 0">
                        <CourseTitleEditCard
                          id={course.id}
                          certificate={course.certificate}
                          name={course.name}
                          subTitle={course.subTitle}
                          duration={course.duration}
                          onFinish={refetch}
                          sx={{ height: '100%' }}
                        />
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item>
                    <CourseBannerEditCard
                      courseId={course.id}
                      bannerURL={course.bannerURL}
                      onFinish={refetch}
                    />
                  </Grid>

                  <Grid item>
                    <Grid container spacing={2}>
                      <Grid item xs={12} md={8}>
                        <CourseIntroductionEditCard
                          id={course.id}
                          introductionVideoURL={course.introductionVideoURL}
                          introductionVideoProcessingStatus={
                            course.introductionVideoProcessingStatus
                          }
                          onFinish={refetch}
                          sx={{ height: '100%' }}
                        />
                      </Grid>
                      <Grid item xs={12} md={4}>
                        <CoursePriceEditCard
                          price={course.price}
                          promotionPrice={course.promotionPrice}
                          publish={course.publish}
                          id={course.id}
                          onFinish={refetch}
                          sx={{ height: '100%' }}
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </ContainerSpacing>
            </TabPanel>

            <TabPanel value={ETab.about}>
              <ContainerSpacing>
                <Grid container direction="row" spacing={2}>
                  <Grid item>
                    <CourseAboutEditCard
                      id={course.id}
                      about={course.about}
                      onFinish={refetch}
                    />
                  </Grid>
                  <Grid item>
                    <CourseOutputEditCard
                      id={course.id}
                      output={course.output}
                      onFinish={refetch}
                    />
                  </Grid>
                </Grid>
              </ContainerSpacing>
            </TabPanel>

            <TabPanel value={ETab.content}>
              <ContainerSpacing maxWidth={false}>
                <Grid item>
                  <Container maxWidth={false}>
                    <CourseContentEditCard courseId={course.id} />
                  </Container>
                </Grid>
              </ContainerSpacing>
            </TabPanel>

            <TabPanel value={ETab.questions}></TabPanel>

            <TabPanel value={ETab.setting}>
              <Container>
                <TeacherCourseSettingPanel course={course} />
              </Container>
            </TabPanel>
          </TabContext>
        </>
      )}
    </>
  );
};

Page.layout = TeacherLayout;

export default Page;
