import { Container, Stack, Typography } from '@mui/material';
import { CreateCourseForm } from '../../../modules/Course/Create/CreateCourseForm';
import { TeacherLayout } from '../../../Layout/TeacherLayout';
import { Messages, setMessage } from '../../../lib/messages';
import { NextPageWithLayout } from '../../../types/components.type';

const Page: NextPageWithLayout = () => {
  const pageTitle = `${Messages.action.create} ${Messages.course.name}`;

  return (
    <>
      <Container maxWidth="md">
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          mb={4}
        >
          <Typography variant="h4" gutterBottom>
            {setMessage(pageTitle)}
          </Typography>
        </Stack>
      </Container>
      <Container maxWidth="sm">
        <CreateCourseForm />
      </Container>
    </>
  );
};

Page.layout = TeacherLayout;

export default Page;
