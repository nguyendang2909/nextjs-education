import { faBook, faQuestion } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Card,
  CardActionArea,
  CardContent,
  Container,
  Grid,
} from '@mui/material';
import { useQuery } from 'react-query';
import { BoxSpacing, BoxSpacingBottom } from '../../components/Box';
import { IconWrapper } from '../../components/Icon';
import { NextLink } from '../../components/Link';
import { AppPage } from '../../components/Page/AppPage';
import { TypographyCenter } from '../../components/Text/Typography';
import { APP_URL } from '../../config';
import { TeacherLayout } from '../../Layout/TeacherLayout';
import { Messages } from '../../lib/messages';
import { specialCharacters } from '../../lib/special-characters';
import { teacherCourseQuestionsService } from '../../lib/teacher-course-questions.service';
import { teacherCoursesService } from '../../lib/teacher-courses.service';
import { NextPageWithLayout } from '../../types/components.type';
import { ECoursePublish } from '../../types/enums';

const Page: NextPageWithLayout = () => {
  const { data: countCourseQuestions } = useQuery(
    'teacherCountCourseQuestions',
    () => teacherCourseQuestionsService.count(),
  );

  const { data: countCourses } = useQuery('countAllTeacherCourses', () =>
    teacherCoursesService.count(),
  );

  const { data: countPublishCourses } = useQuery('countAllPublishCourses', () =>
    teacherCoursesService.count({ publish: ECoursePublish.Published }),
  );

  return (
    <AppPage title="Quản lý giảng dạy">
      <Container>
        <BoxSpacingBottom>Bạn có:</BoxSpacingBottom>
        <BoxSpacing>
          <Grid container spacing={2}>
            <Grid item xs={6} sm={4} md={3}>
              <Card>
                <NextLink href={APP_URL.teacher.courses}>
                  <CardActionArea>
                    <CardContent>
                      <IconWrapper>
                        <FontAwesomeIcon icon={faBook} />
                      </IconWrapper>

                      <TypographyCenter
                        variant="h3"
                        noWrap
                        sx={{
                          fontWeight: 'bold',
                        }}
                      >
                        {countPublishCourses} / {countCourses}{' '}
                        {Messages.course.name}
                      </TypographyCenter>
                      <TypographyCenter>{'đã được đăng tải'}</TypographyCenter>
                    </CardContent>
                  </CardActionArea>
                </NextLink>
              </Card>
            </Grid>
            <Grid item xs={6} sm={4} md={3}>
              <Card>
                <NextLink href={APP_URL.teacher.courseQuestions}>
                  <CardActionArea>
                    <CardContent>
                      <IconWrapper>
                        <FontAwesomeIcon icon={faQuestion} />
                      </IconWrapper>

                      <TypographyCenter
                        variant="h3"
                        noWrap
                        sx={{
                          fontWeight: 'bold',
                        }}
                      >
                        {countCourseQuestions} {Messages.course.question}
                      </TypographyCenter>
                      <TypographyCenter>
                        {specialCharacters.space}
                      </TypographyCenter>
                    </CardContent>
                  </CardActionArea>
                </NextLink>
              </Card>
            </Grid>
          </Grid>
        </BoxSpacing>
      </Container>
    </AppPage>
  );
};

Page.layout = TeacherLayout;

export default Page;
