import {
  Avatar,
  Box,
  Button,
  Container,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from '@mui/material';
import Head from 'next/head';
import { Fragment, useEffect } from 'react';
import { useQuery } from 'react-query';
import { AdminLayout } from '../../../Layout/AdminLayout';
import { adminUsersService } from '../../../lib/admin-users.service';
import { Messages, messagesService, setMessage } from '../../../lib/messages';
import { NextPageWithLayout } from '../../../types/components.type';
import { useRouter } from 'next/router';
import { requestAPI, requestService } from '../../../lib/request';
import { Formatter } from '../../../lib/formatter';
import { ERegisterTeacher, ERole } from '../../../types/enums';
import { TablePopoverFilter } from '../../../components/Table/TablePopoverFilter';
import { AFindAllUsersDto } from '../../../types/request.dto';
import { urlQueryService } from '../../../lib/url-query.service';
import {
  TableCellRecordStatus,
  TableHeaderCellRecordStatus,
} from '../../../modules/Record';
import { StackSpaceBetween } from '../../../components/Stack';
import { TableCellIndex } from '../../../components/Table/TableCell';
import { ContainerSpacing } from '../../../components/Container';
import { AdminUserSearchFields } from '../../../modules/User/AdminUserSearchFields';
import { LoadingContainer } from '../../../components/Container/LoadingContainer';

const Page: NextPageWithLayout = () => {
  const pageTitle = `${Messages.action.manage} ${Messages.user.name}`;

  const router = useRouter();

  const routerOptions = { router };

  const queryOptions = { query: router.query };

  const role = urlQueryService.getOne('role', queryOptions);

  const isActive = urlQueryService.getOneAsBoolean('isActive', queryOptions);

  const email = urlQueryService.getOne('email', queryOptions);

  const phoneNumber = urlQueryService.getOne('phoneNumber', queryOptions);

  const fullname = urlQueryService.getOne('fullname', queryOptions);

  const registerTeacher = urlQueryService.getOne(
    'registerTeacher',
    queryOptions,
  );

  const { pageSize, currentPage } = urlQueryService.getPaginated(queryOptions);

  const findOptions: AFindAllUsersDto = {
    isActive,
    role,
    registerTeacher,
    email,
    phoneNumber,
    fullname,
  };

  const findManyOptions = {
    ...findOptions,
    currentPage,
    pageSize,
  };

  const {
    data: users = [],
    refetch: refetchUsers,
    isFetching,
  } = useQuery(
    'admin.users',
    () => adminUsersService.getMany(findManyOptions),
    {
      enabled: router.isReady,
      staleTime: Infinity,
    },
  );

  const { data: countUsers = 0, refetch: refetchCountUsers } = useQuery(
    'admin.countUser',
    () => adminUsersService.count(findOptions),
    {
      enabled: router.isReady,
      staleTime: Infinity,
    },
  );

  useEffect(() => {
    if (router.isReady) {
      refetchUsers();
    }
  }, [
    router.isReady,
    refetchUsers,
    pageSize,
    currentPage,
    isActive,
    role,
    registerTeacher,
    email,
    phoneNumber,
    fullname,
  ]);

  useEffect(() => {
    if (router.isReady) {
      refetchCountUsers();
    }
  }, [
    router.isReady,
    refetchCountUsers,
    pageSize,
    currentPage,
    isActive,
    role,
    registerTeacher,
    email,
    phoneNumber,
    fullname,
  ]);

  const pagination = (
    <TablePagination
      page={currentPage - 1}
      count={countUsers}
      rowsPerPage={pageSize}
      onPageChange={(event, page) => {
        urlQueryService.setUrlQuery({ currentPage: page + 1 }, routerOptions);
      }}
    ></TablePagination>
  );

  return (
    <>
      <Head>
        <title>{messagesService.setPageTitle(pageTitle)}</title>
      </Head>

      <Container>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <Typography variant="h1" gutterBottom>
            {setMessage(pageTitle)}
          </Typography>
        </Stack>
      </Container>

      <ContainerSpacing>
        <AdminUserSearchFields />
      </ContainerSpacing>

      <TableContainer>
        {isFetching && <LoadingContainer />}
        <Table>
          <TableHead>
            <TableRow>{pagination}</TableRow>
            <TableRow>
              <TableCellIndex />
              <TableCell>{setMessage(Messages.user.avatar)}</TableCell>
              <TableCell>{setMessage(Messages.user.email)}</TableCell>
              <TableCell>{setMessage(Messages.user.phoneNumber)}</TableCell>
              <TableCell>{setMessage(Messages.user.fullname)}</TableCell>
              <TableCell>
                <StackSpaceBetween>
                  <Box>{setMessage(Messages.user.role)}</Box>
                  <Box>
                    <TablePopoverFilter
                      options={[
                        {
                          text: Formatter.formatRole(ERole.Member),
                          value: ERole.Member,
                        },
                        {
                          text: Formatter.formatRole(ERole.Teacher),
                          value: ERole.Teacher,
                        },
                        {
                          text: Formatter.formatRole(ERole.Admin),
                          value: ERole.Admin,
                        },
                      ]}
                      urlQueryName="role"
                    />
                  </Box>
                </StackSpaceBetween>
              </TableCell>
              <TableCell>
                <StackSpaceBetween>
                  <Box>Đăng ký giảng</Box>
                  <Box>
                    <TablePopoverFilter
                      options={[
                        {
                          text: Formatter.formatRegisterTeacher(
                            ERegisterTeacher.notRegistered,
                          ),
                          value: ERegisterTeacher.notRegistered,
                        },
                        {
                          text: Formatter.formatRegisterTeacher(
                            ERegisterTeacher.pending,
                          ),
                          value: ERegisterTeacher.pending,
                        },
                        {
                          text: Formatter.formatRegisterTeacher(
                            ERegisterTeacher.accept,
                          ),
                          value: ERegisterTeacher.accept,
                        },
                        {
                          text: Formatter.formatRegisterTeacher(
                            ERegisterTeacher.reject,
                          ),
                          value: ERegisterTeacher.reject,
                        },
                      ]}
                      urlQueryName="registerTeacher"
                    />
                  </Box>
                </StackSpaceBetween>
              </TableCell>
              <TableHeaderCellRecordStatus />
            </TableRow>
          </TableHead>

          <TableBody>
            {users.map((user, index) => {
              const rowId = pageSize * (currentPage - 1) + index + 1;

              const userId = user.id;

              return (
                <Fragment key={index}>
                  {userId && (
                    <TableRow>
                      <TableCell>
                        <Typography>{rowId}</Typography>
                      </TableCell>
                      <TableCell>
                        <Avatar
                          src={
                            user.avatarURL &&
                            requestService.getURL(user.avatarURL)
                          }
                          alt="avatar"
                        >
                          {user.fullname && user.fullname[0]}
                        </Avatar>
                      </TableCell>
                      <TableCell>{user.email || ''}</TableCell>
                      <TableCell>{user.phoneNumber || ''}</TableCell>
                      <TableCell>{user.fullname || ''}</TableCell>
                      <TableCell>
                        {user.role
                          ? setMessage(Formatter.formatRole(user.role))
                          : ''}
                      </TableCell>
                      <TableCell>
                        {user.registerTeacher
                          ? Formatter.formatRegisterTeacher(
                              user.registerTeacher,
                            )
                          : ''}
                        {user.registerTeacher === ERegisterTeacher.pending && (
                          <Button
                            onClick={async () => {
                              await adminUsersService.update(userId, {
                                registerTeacher: ERegisterTeacher.accept,
                              });

                              refetchUsers();

                              refetchCountUsers();
                            }}
                          >
                            Duyệt
                          </Button>
                        )}
                      </TableCell>
                      <TableCellRecordStatus
                        isActive={user.isActive}
                        id={userId}
                        onChange={adminUsersService.update}
                        refetch={refetchUsers}
                      />
                    </TableRow>
                  )}
                </Fragment>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

Page.layout = AdminLayout;

export default Page;
