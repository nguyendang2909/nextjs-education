import { faBan, faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Box,
  Container,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from '@mui/material';
import { useRouter } from 'next/router';
import { Fragment, useEffect } from 'react';
import { useQuery } from 'react-query';
import { LoadingContainer } from '../../../components/Container/LoadingContainer';
import { MoreMenu } from '../../../components/Menu/MoreMenu';
import { NotFoundContainer } from '../../../components/NotFound/NotFoundContainer';
import { AppPage } from '../../../components/Page/AppPage';
import { StackSpaceBetween } from '../../../components/Stack';
import { TableCellIndex } from '../../../components/Table/TableCell';
import { TablePopoverFilter } from '../../../components/Table/TablePopoverFilter';
import { SortDirection } from '../../../components/Table/TableSort';
import { AdminLayout } from '../../../Layout/AdminLayout';
import { adminOrdersService } from '../../../lib/admin-orders.service';
import { Formatter } from '../../../lib/formatter';
import { Messages, setMessage } from '../../../lib/messages';
import { notificationService } from '../../../lib/notificationService';
import { urlQueryService } from '../../../lib/url-query.service';
import { AdminOrderSearchFields } from '../../../modules/Order/AdminOrderSearchFields';
import {
  TableCellRecordStatus,
  TableHeaderCellRecordStatus,
} from '../../../modules/Record';
import { TableHeaderCellCreatedAt } from '../../../modules/Record/TableCellCreatedAt';
import { NextPageWithLayout } from '../../../types/components.type';
import { EPaymentMethod } from '../../../types/enums';
import {
  AFindAllOrdersDto,
  AFindManyOrdersDto,
} from '../../../types/request.dto';

const Page: NextPageWithLayout = () => {
  const pageTitle = Messages.cart.order;

  const router = useRouter();

  const routerOptions = { router };

  const queryOptions = { query: router.query };

  const isActive = urlQueryService.getOneAsBoolean('isActive', queryOptions);

  const paid = urlQueryService.getOneAsBoolean('paid', queryOptions);

  const id = urlQueryService.getOneAsNumber('id', queryOptions);

  const sortCreatedAt = urlQueryService.getOne(
    'sortCreatedAt',
    queryOptions,
  ) as SortDirection;

  const paymentMethod = urlQueryService.getOne(
    'paymentMethod',
    queryOptions,
  ) as EPaymentMethod;

  const { pageSize, currentPage } = urlQueryService.getPaginated(queryOptions);

  const findAllOptions: AFindAllOrdersDto = {
    isActive,
    paymentMethod,
    id,
  };

  const findManyOptions: AFindManyOrdersDto = {
    ...findAllOptions,
    currentPage,
    pageSize,
    sortCreatedAt,
  };

  const {
    data: orders,
    refetch: refetchOrders,
    isSuccess,
    isError,
    isLoading,
    isFetching,
  } = useQuery(
    'admin.orders',
    () => adminOrdersService.getMany(findManyOptions),
    {
      enabled: router.isReady,
      staleTime: Infinity,
    },
  );

  const { data: countOrders, refetch: refetchCountOrders } = useQuery(
    'admin.countOrders',
    () => adminOrdersService.count(findAllOptions),
    {
      enabled: router.isReady,
      staleTime: Infinity,
    },
  );

  const pagination = (
    <TablePagination
      page={currentPage - 1}
      count={countOrders || 0}
      rowsPerPage={pageSize}
      onPageChange={(event, page) => {
        urlQueryService.setUrlQuery({ currentPage: page + 1 }, routerOptions);
      }}
    ></TablePagination>
  );

  useEffect(() => {
    if (router.isReady) {
      refetchOrders();
    }
  }, [
    router.isReady,
    refetchCountOrders,
    pageSize,
    currentPage,
    sortCreatedAt,
    isActive,
    paymentMethod,
    refetchOrders,
    paid,
    id,
  ]);

  useEffect(() => {
    if (router.isReady) {
      refetchCountOrders();
    }
  }, [router.isReady, refetchCountOrders, isActive, paymentMethod, paid, id]);

  const handleChangeAcceptOrder = async (id: number) => {
    try {
      await adminOrdersService.update(id, { paid: true });

      refetchOrders();
    } catch (err) {
      notificationService.handleError(err);
    }
  };

  return (
    <AppPage title={pageTitle} header={{}}>
      {isError && <NotFoundContainer />}

      {isLoading && <LoadingContainer />}

      {isSuccess && !!orders && (
        <>
          <Container>
            <Grid>
              <AdminOrderSearchFields isSubmitting={isFetching} />
            </Grid>
          </Container>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>{pagination}</TableRow>
                <TableRow>
                  <TableCellIndex />
                  <TableCell>{setMessage(Messages.cart.orderId)}</TableCell>
                  <TableCell>
                    <StackSpaceBetween>
                      <Box>{setMessage(Messages.cart.paymentMethod)}</Box>
                      <Box>
                        <TablePopoverFilter
                          urlQueryName="paymentMethod"
                          options={[
                            {
                              text: Formatter.formatPaymentMethod(
                                EPaymentMethod.Momo,
                              ),
                              value: EPaymentMethod.Momo,
                            },
                            {
                              text: Formatter.formatPaymentMethod(
                                EPaymentMethod.MoneyTransfer,
                              ),
                              value: EPaymentMethod.MoneyTransfer,
                            },
                          ]}
                        />
                      </Box>
                    </StackSpaceBetween>
                  </TableCell>
                  <TableCell>{setMessage(Messages.cart.price)}</TableCell>
                  <TableCell>{setMessage(Messages.cart.checkout)}</TableCell>
                  <TableHeaderCellCreatedAt />
                  <TableHeaderCellRecordStatus />
                </TableRow>
              </TableHead>

              <TableBody>
                {orders.map((order, index) => {
                  const rowId = pageSize * (currentPage - 1) + index + 1;

                  const { id: orderId } = order;

                  return (
                    <Fragment key={index}>
                      {!!orderId && (
                        <TableRow>
                          <TableCell>
                            <Typography>{rowId}</Typography>
                          </TableCell>
                          <TableCell>
                            {Formatter.formatOrderId(orderId)}
                          </TableCell>
                          <TableCell>
                            {!!order.paymentMethod &&
                              Formatter.formatPaymentMethod(
                                order.paymentMethod,
                              )}
                          </TableCell>
                          <TableCell>
                            {!!order.totalPrice &&
                              Formatter.formatMoney(order.totalPrice)}
                          </TableCell>

                          <TableCell>
                            <StackSpaceBetween>
                              <Box>
                                <FontAwesomeIcon
                                  icon={order.paid ? faCheck : faBan}
                                />
                              </Box>
                              <Box>
                                <MoreMenu
                                  items={[
                                    {
                                      name: Messages.action.accept,
                                      icon: faCheck,
                                      onClick: () => {
                                        handleChangeAcceptOrder(orderId);
                                      },
                                    },
                                  ]}
                                />
                              </Box>
                            </StackSpaceBetween>
                          </TableCell>
                          <TableCell>
                            {!!order.createdAt &&
                              Formatter.formatTime(order.createdAt)}
                          </TableCell>
                          <TableCellRecordStatus
                            isActive={order.isActive}
                            id={orderId}
                            onChange={adminOrdersService.update}
                            refetch={refetchOrders}
                          />
                        </TableRow>
                      )}
                    </Fragment>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
    </AppPage>
  );
};

Page.layout = AdminLayout;

export default Page;
