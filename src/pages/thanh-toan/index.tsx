import {
  faCreditCard,
  faMoneyBillTransfer,
} from '@fortawesome/free-solid-svg-icons';
import {
  Box,
  Breadcrumbs,
  Card,
  CardContent,
  CardHeader,
  Collapse,
  Divider,
  FormControlLabel,
  Grid,
  List,
  Paper,
  Radio,
  RadioGroup,
  Stack,
  Typography,
} from '@mui/material';
import Head from 'next/head';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { Fragment } from 'react';
import { useQuery } from 'react-query';
import { BoxSpacingBig, BoxSpacingBottom } from '../../components/Box';
import { Breadcrumb, BreadcrumbHome } from '../../components/Breadcrumbs';
import {
  ContainerSpacing,
  ContainerSpacingBottom,
} from '../../components/Container';
import { LoadingContainer } from '../../components/Container/LoadingContainer';
import { NotFoundContent } from '../../components/NotFound';
import { PaymentStepper } from '../../components/Payment/PaymentStepper';
import { cartsService } from '../../lib/carts.service';
import {
  appShortTitle,
  Messages,
  messagesService,
  setMessage,
} from '../../lib/messages';
import { NextPageWithLayout } from '../../types/components.type';
import momoPayment from '../../../public/static/images/payments/momo.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useFormik } from 'formik';
import { LoadingButton } from '@mui/lab';
import { urlQueryService } from '../../lib/url-query.service';
import { NotFoundContainer } from '../../components/NotFound/NotFoundContainer';
import { ListItemPaddingReponsive } from '../gio-hang';
import { Formatter } from '../../lib/formatter';
import { PrivateMainLayout } from '../../Layout/PrivateMainLayout';
import { ordersService } from '../../lib/orders.servie';
import { CreateOrderParams } from '../../types/form-params.type';
import { EPaymentMethod } from '../../types/enums';
import { toast } from 'react-toastify';
import { APP_URL } from '../../config';
import { notificationService } from '../../lib/notificationService';
import { CartItemsTable } from '../../modules/Cart/CartItemsTable';

const Page: NextPageWithLayout = () => {
  const router = useRouter();

  const pageTitle = setMessage(Messages.action.pay);

  const payCartId = urlQueryService.getOneOrManyAsNumber('cartId', {
    query: router.query,
  });

  const {
    data: cartPriceData,
    isSuccess,
    isError,
    isLoading,
  } = useQuery(
    'cartPrice',
    () => cartsService.getPrice({ id: payCartId, paid: false }),
    { enabled: router.isReady },
  );

  const { data: payCarts } = useQuery(
    'payCarts',
    () => cartsService.getMany({ id: payCartId, paid: false }),
    { enabled: router.isReady },
  );

  const { getFieldProps, handleSubmit, isSubmitting, values } =
    useFormik<CreateOrderParams>({
      initialValues: {
        paymentMethod: EPaymentMethod.MoneyTransfer,
      },
      onSubmit: async value => {
        try {
          if (!payCarts) {
            toast.error('Bạn chưa chọn khoá học');

            return;
          }

          const cartId = payCarts
            .map(cartItem => cartItem.id)
            .filter(item => !!item) as number[];

          const created = await ordersService.create({
            ...value,
            cartId: cartId,
          });

          if (created.paymentMethod && created.id) {
            switch (created.paymentMethod) {
              case EPaymentMethod.Momo:
                break;

              default:
                router.push(
                  `${APP_URL.checkoutMoneyTransfer}/${Formatter.formatOrderId(
                    created.id,
                  )}`,
                );
                break;
            }
          }
        } catch (err) {
          notificationService.handleError(err);
        }
      },
    });

  return (
    <>
      <Head>
        <title>{messagesService.setPageTitle(pageTitle)}</title>
      </Head>
      <ContainerSpacingBottom>
        <Breadcrumbs>
          <BreadcrumbHome />
          <Breadcrumb title={pageTitle} icon={faCreditCard} />
        </Breadcrumbs>
        <Typography variant="h1">{pageTitle}</Typography>
      </ContainerSpacingBottom>

      {isError && (
        <ContainerSpacing>
          <NotFoundContent />
        </ContainerSpacing>
      )}

      {isLoading && <LoadingContainer />}

      {isError && <NotFoundContainer />}

      {isSuccess && (
        <>
          {!!cartPriceData ? (
            <>
              <ContainerSpacing>
                <form noValidate onSubmit={handleSubmit}>
                  <Stack
                    direction={{
                      xs: 'column',
                      md: 'row',
                    }}
                    justifyContent="space-between"
                    spacing={3}
                  >
                    <Grid item xs={12} md={8}>
                      <BoxSpacingBottom>
                        <PaymentStepper step={1} />
                      </BoxSpacingBottom>

                      <BoxSpacingBig>
                        <Typography variant="h3" gutterBottom>
                          Phương thức thanh toán
                        </Typography>

                        <RadioGroup {...getFieldProps('paymentMethod')}>
                          <FormControlLabel
                            value={EPaymentMethod.MoneyTransfer}
                            control={<Radio />}
                            label={
                              <Stack direction="row" spacing={2}>
                                <Box>
                                  <FontAwesomeIcon
                                    icon={faMoneyBillTransfer}
                                    fontSize={24}
                                  />
                                </Box>
                                <Typography>Chuyển khoản ngân hàng</Typography>
                              </Stack>
                            }
                          />

                          <Collapse
                            in={
                              values.paymentMethod ===
                              EPaymentMethod.MoneyTransfer
                            }
                          >
                            <Paper sx={{ padding: 2, marginBottom: 3 }}>
                              <Typography>
                                Khóa học sẽ được kích hoạt sau khi{' '}
                                {appShortTitle} kiểm tra tài khoản và xác nhận
                                việc thanh toán của bạn thành công.
                                <br />
                                (Thời gian kiểm tra và xác nhận tài khoản ít
                                nhất là 1 giờ)
                              </Typography>
                            </Paper>
                          </Collapse>

                          <FormControlLabel
                            value={EPaymentMethod.Momo}
                            label={
                              <Stack direction="row" spacing={2}>
                                <Box>
                                  <Image
                                    src={momoPayment}
                                    // layout="responsive"
                                    width={30}
                                    height={30}
                                    alt="Momo"
                                  />
                                </Box>
                                <Typography>Ví Momo</Typography>
                              </Stack>
                            }
                            control={<Radio />}
                          />
                          <Collapse
                            in={values.paymentMethod === EPaymentMethod.Momo}
                          >
                            <Paper sx={{ padding: 2, marginBottom: 3 }}>
                              <Typography>
                                Sau khi nhấn Thanh toán, hệ thống sẽ hiển thị mã
                                QR kèm hướng dẫn.
                                <br />
                                Bạn cần tải và cài ứng dụng Momo trên điện thoại
                                và sử dụng để quét mã QR trên để thanh toán.
                              </Typography>
                            </Paper>
                          </Collapse>
                        </RadioGroup>
                      </BoxSpacingBig>

                      <Divider />

                      {!!payCarts && (
                        <BoxSpacingBig>
                          <CartItemsTable carts={payCarts} />
                        </BoxSpacingBig>
                      )}
                    </Grid>

                    <Grid
                      paddingBottom={{
                        xs: 5,
                        md: 0,
                      }}
                      minWidth={{
                        xs: '100%',
                        md: '400px',
                      }}
                    >
                      <Card>
                        <CardHeader
                          title={setMessage(Messages.cart.bill)}
                        ></CardHeader>
                        <CardContent>
                          <List
                            sx={{ fontSize: '1.3rem', color: 'text.secondary' }}
                          >
                            <ListItemPaddingReponsive divider>
                              <Grid container>
                                <Box>Giá</Box>
                                <Box sx={{ textAlign: 'right', flexGrow: 1 }}>
                                  {!!cartPriceData.price
                                    ? Formatter.formatMoney(cartPriceData.price)
                                    : 0}
                                </Box>
                              </Grid>
                            </ListItemPaddingReponsive>
                            <ListItemPaddingReponsive divider>
                              <Grid container>
                                <Box>
                                  {setMessage(Messages.course.savePrice)}
                                </Box>
                                <Box sx={{ textAlign: 'right', flexGrow: 1 }}>
                                  {!!cartPriceData.savePrice
                                    ? Formatter.formatMoney(
                                        cartPriceData.savePrice,
                                      )
                                    : 0}
                                </Box>
                              </Grid>
                            </ListItemPaddingReponsive>
                            <ListItemPaddingReponsive>
                              <Grid container sx={{ color: 'text.primary' }}>
                                <Box>
                                  {setMessage(Messages.cart.totalPayment)}
                                </Box>
                                <Box
                                  sx={{
                                    textAlign: 'right',
                                    flexGrow: 1,
                                    fontWeight: 700,
                                  }}
                                >
                                  {!!cartPriceData.totalPrice
                                    ? Formatter.formatMoney(
                                        cartPriceData.totalPrice,
                                      )
                                    : 0}
                                </Box>
                              </Grid>
                            </ListItemPaddingReponsive>
                          </List>

                          <LoadingButton
                            type="submit"
                            loading={isSubmitting}
                            size="large"
                            variant="contained"
                            fullWidth
                            disabled={!payCarts}
                          >
                            {Messages.action.pay}
                          </LoadingButton>
                        </CardContent>
                      </Card>
                    </Grid>
                  </Stack>

                  <Grid
                    paddingBottom={{
                      xs: 5,
                      md: 0,
                    }}
                    minWidth={{
                      xs: '100%',
                      md: '400px',
                    }}
                  ></Grid>
                </form>
              </ContainerSpacing>
            </>
          ) : (
            <></>
          )}
        </>
      )}
    </>
  );
};

Page.layout = PrivateMainLayout;

export default Page;
