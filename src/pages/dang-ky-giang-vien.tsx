import { Box, Card, Container, Typography } from '@mui/material';
import Head from 'next/head';
import Image from 'next/image';
import { BoxBackground, BoxCenter, BoxSpacing } from '../components/Box';
import { ButtonWhite } from '../components/Button';
import { CardContentCenter, CardFullHeight } from '../components/Card';
import { GridPadding, GridStrech } from '../components/Grid';
import { NextLink } from '../components/Link';
import {
  TypographyCenterShadowWhite,
  TypographyCenterUppercase,
} from '../components/Text/Typography';
import { APP_URL } from '../config';
import { appShortTitle, Messages, messagesService } from '../lib/messages';
import { NextPageWithLayout } from '../types/components.type';

const Page: NextPageWithLayout = () => {
  const pageTitle = `Hợp tác giảng dạy cùng ${appShortTitle}`;

  return (
    <>
      <Head>
        <title>{messagesService.setPageTitle(pageTitle)}</title>
      </Head>
      <BoxBackground
        sx={{
          marginTop: -2,
          backgroundImage:
            'url("/static/images/backgrounds/dang-ky-giang-vien-1.jpg")',
        }}
      >
        <Container
          sx={{
            height: '400px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Box>
            <BoxSpacing>
              <TypographyCenterShadowWhite
                variant="h1"
                sx={{ textAlign: 'center' }}
              >
                {pageTitle}
              </TypographyCenterShadowWhite>
            </BoxSpacing>
            <BoxSpacing>
              <TypographyCenterShadowWhite>
                {'“'}MỖI CHÚNG TA ĐỀU CÓ MỘT SƯ MỆNH CHIA SẺ LẠI GIÁ TRỊ CHO THẾ
                HỆ SAU{'”'}
              </TypographyCenterShadowWhite>
            </BoxSpacing>
            <BoxSpacing>
              <BoxCenter>
                <NextLink href={APP_URL.teacher.register}>
                  <ButtonWhite
                    variant="outlined"
                    size="large"
                    sx={{ width: '200px' }}
                  >
                    {Messages.action.registerNow}
                  </ButtonWhite>
                </NextLink>
              </BoxCenter>
            </BoxSpacing>
          </Box>
        </Container>
      </BoxBackground>

      <Box>
        <BoxSpacing>
          <Container
            sx={{
              height: '400px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Box>
              <TypographyCenterUppercase variant="h2">
                5 LÝ DO NÊN GIẢNG DẠY TRÊN {appShortTitle}
              </TypographyCenterUppercase>
              <GridStrech container>
                <GridPadding item xs={12} sm={6} md={12 / 5}>
                  <CardFullHeight>
                    <CardContentCenter>
                      <Box>
                        <Image
                          alt="lam 1 lan ban n lan"
                          src="/static/images/hop-tac/lam-1-lan-ban-n-lan.png"
                          width={100}
                          height={100}
                        />
                      </Box>
                      <Typography>Làm 1 lần, Bán n lần</Typography>
                    </CardContentCenter>
                  </CardFullHeight>
                </GridPadding>
                <GridPadding item xs={12} sm={6} md={12 / 5}>
                  <Card sx={{ height: '100%' }}>
                    <CardContentCenter>
                      <Box>
                        <Image
                          alt="quang ba"
                          src="/static/images/hop-tac/quang-ba.png"
                          width={100}
                          height={100}
                        />
                      </Box>
                      <Typography>Quảng bá tới hàng triệu học viên</Typography>
                    </CardContentCenter>
                  </Card>
                </GridPadding>
                <GridPadding item xs={12} sm={6} md={12 / 5}>
                  <Card sx={{ height: '100%' }}>
                    <CardContentCenter>
                      <Box>
                        <Image
                          alt="chia se loi nhuan"
                          src="/static/images/hop-tac/chia-se-loi-nhuan.png"
                          width={100}
                          height={100}
                        />
                      </Box>
                      <Typography>Chia sẻ lợi nhuận 30%-100%</Typography>
                    </CardContentCenter>
                  </Card>
                </GridPadding>
                <GridPadding item xs={12} sm={6} md={12 / 5}>
                  <Card sx={{ height: '100%' }}>
                    <CardContentCenter>
                      <Box>
                        <Image
                          alt="thanh toan"
                          src="/static/images/hop-tac/thanh-toan.png"
                          width={100}
                          height={100}
                        />
                      </Box>
                      <Typography>
                        Thanh toán trước ngày 15 hàng tháng
                      </Typography>
                    </CardContentCenter>
                  </Card>
                </GridPadding>
                <GridPadding item xs={12} sm={6} md={12 / 5}>
                  <Card sx={{ height: '100%' }}>
                    <CardContentCenter>
                      <Box>
                        <Image
                          alt="hoc online"
                          src="/static/images/hop-tac/hoc-qua-may-tinh.png"
                          width={100}
                          height={100}
                        />
                      </Box>
                      <Typography>
                        Học qua VOD, Live và offline event
                      </Typography>
                    </CardContentCenter>
                  </Card>
                </GridPadding>
              </GridStrech>
            </Box>
          </Container>
        </BoxSpacing>
      </Box>
    </>
  );
};

export default Page;
