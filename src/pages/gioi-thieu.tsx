import {
  Card,
  CardContent,
  Container,
  Grid,
  Typography,
  useTheme,
} from '@mui/material';
import { Box } from '@mui/system';
import Head from 'next/head';
import Image from 'next/image';
import { GridStrech } from '../components/Grid';
import {
  TypographyCenter,
  TypographyCenterShadowWhite,
  TypographyOneLine,
  TypographyShadowWhite,
} from '../components/Text/Typography';
import {
  appShortTitle,
  Messages,
  messagesService,
  setMessage,
} from '../lib/messages';
import { NextPageWithLayout } from '../types/components.type';

const Page: NextPageWithLayout = () => {
  const theme = useTheme();

  return (
    <>
      <Head>
        <title>{messagesService.setPageTitle('Giới thiệu')}</title>
      </Head>

      <Box
        sx={{
          marginTop: -2,
          backgroundImage: 'url("/static/images/backgrounds/world.jpg")',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          padding: theme.spacing(4, 0),
        }}
      >
        <Container>
          <Box
            sx={{
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              minHeight: '400px',
            }}
          >
            <Box>
              <TypographyCenterShadowWhite
                variant="h1"
                sx={{
                  padding: theme.spacing(2, 0),
                }}
              >
                Học viện Online {setMessage(Messages.app.shortTitle)}
              </TypographyCenterShadowWhite>
              <TypographyShadowWhite
                variant="h3"
                sx={{
                  lineHeight: '3rem',
                  textAlign: 'center',
                }}
              >
                Chia sẻ kiến thức và kinh nghiệm thực tế tới hàng triệu người
              </TypographyShadowWhite>
              <Typography
                variant="h3"
                sx={{
                  color: '#fff',
                  textShadow: '0px 2px 3px rgb(0 0 0 / 70%)',
                  textAlign: 'center',
                  textTransform: 'uppercase',
                  padding: theme.spacing(2, 0),
                }}
              >
                học mọi lúc mọi nơi
              </Typography>
            </Box>
          </Box>
        </Container>
      </Box>

      <Box sx={{ padding: theme.spacing(3, 0) }}>
        <Container>
          <Box
            sx={{
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              minHeight: '400px',
            }}
          >
            <Box sx={{ flexGrow: 1 }}>
              <Box sx={{ pb: 3 }}>
                <Typography
                  variant="h2"
                  sx={{
                    textAlign: 'center',
                    textTransform: 'uppercase',
                    padding: theme.spacing(2, 0),
                  }}
                >
                  Giới thiệu - Học viện Online {Messages.app.shortTitle}
                </Typography>
                <Typography sx={{ textAlign: 'center', lineHeight: '3rem' }}>
                  {appShortTitle} là một hệ thống đào tạo trực tuyến, cổng kết
                  nối Chuyên gia với Học viên
                </Typography>
                <Typography sx={{ textAlign: 'center', lineHeight: '3rem' }}>
                  Sứ mệnh của {appShortTitle} là chia sẻ kiến thức thực tiễn tới
                  10 triệu người dân Việt Nam
                </Typography>
              </Box>
              <GridStrech container spacing={2}>
                <Grid item xs={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent>
                      <TypographyOneLine
                        sx={{
                          fontSize: 32,
                          textAlign: 'center',
                          fontWeight: 600,
                        }}
                      >
                        500.000+
                      </TypographyOneLine>
                      <TypographyOneLine
                        sx={{
                          textAlign: 'center',
                        }}
                      >
                        {setMessage(Messages.student.name)}
                      </TypographyOneLine>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent>
                      <TypographyOneLine
                        sx={{
                          fontSize: 32,
                          textAlign: 'center',
                          fontWeight: 600,
                        }}
                      >
                        1000+
                      </TypographyOneLine>
                      <TypographyOneLine
                        sx={{
                          textAlign: 'center',
                        }}
                      >
                        {setMessage(Messages.teacher.name)}
                      </TypographyOneLine>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent>
                      <TypographyOneLine
                        sx={{
                          fontSize: 32,
                          textAlign: 'center',
                          fontWeight: 600,
                        }}
                      >
                        2000+
                      </TypographyOneLine>
                      <TypographyOneLine
                        sx={{
                          textAlign: 'center',
                        }}
                      >
                        {setMessage(Messages.course.name)}
                      </TypographyOneLine>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent>
                      <TypographyOneLine
                        sx={{
                          fontSize: 32,
                          textAlign: 'center',
                          fontWeight: 600,
                        }}
                      >
                        80.000+
                      </TypographyOneLine>
                      <TypographyOneLine sx={{ textAlign: 'center' }}>
                        {setMessage('chi nhánh')}
                      </TypographyOneLine>
                    </CardContent>
                  </Card>
                </Grid>
              </GridStrech>
            </Box>
          </Box>
        </Container>
      </Box>

      <Box sx={{ padding: theme.spacing(3, 0), background: '#eee' }}>
        <Container>
          <Box
            sx={{
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              minHeight: '400px',
            }}
          >
            <Box sx={{ flexGrow: 1 }}>
              <Box pb={3}>
                <Typography
                  variant="h2"
                  sx={{
                    textAlign: 'center',
                    textTransform: 'uppercase',
                    lineHeight: '3rem',
                    padding: theme.spacing(2, 0),
                  }}
                >
                  Trải nghiệm phương pháp học tập hiện đại
                </Typography>
              </Box>
              <GridStrech container spacing={2}>
                <Grid item xs={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent sx={{ textAlign: 'center' }}>
                      <Image
                        alt="hoc moi luc moi noi"
                        src="/static/images/cards/hoc-moi-luc-moi-noi.png"
                        width={100}
                        height={100}
                      />
                      <TypographyCenter>Học mọi lúc, mọi nơi</TypographyCenter>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent sx={{ textAlign: 'center' }}>
                      <Image
                        alt="chi phi hop ly"
                        src="/static/images/cards/chi-phi-hop-ly.png"
                        width={100}
                        height={100}
                      />
                      <TypographyCenter>Chi phí hợp lý</TypographyCenter>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent sx={{ textAlign: 'center' }}>
                      <Image
                        alt="tinh ung dung cao"
                        src="/static/images/cards/tinh-ung-dung-cao.png"
                        width={100}
                        height={100}
                      />
                      <TypographyCenter>Tính ứng dụng cao</TypographyCenter>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent sx={{ textAlign: 'center' }}>
                      <Image
                        alt="tuong tac voi giang vien"
                        src="/static/images/cards/tuong-tac-voi-giang-vien.png"
                        width={100}
                        height={100}
                      />
                      <TypographyCenter>
                        Tương tác với giảng viên
                      </TypographyCenter>
                    </CardContent>
                  </Card>
                </Grid>
              </GridStrech>
            </Box>
          </Box>
        </Container>
      </Box>

      <Box sx={{ padding: theme.spacing(3, 0) }}>
        <Container>
          <Box
            sx={{
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              minHeight: '400px',
            }}
          >
            <Box sx={{ flexGrow: 1 }}>
              <Box pb={3}>
                <Typography
                  variant="h2"
                  sx={{
                    textAlign: 'center',
                    textTransform: 'uppercase',
                    lineHeight: '3rem',
                    padding: theme.spacing(2, 0),
                  }}
                >
                  Lợi thế mô hình đào tạo E-Learning
                </Typography>
              </Box>
              <GridStrech container spacing={2}>
                <Grid item xs={12} sm={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent sx={{ textAlign: 'center' }}>
                      <TypographyCenter variant="h3">
                        Cơ hội nghề nghiệp
                      </TypographyCenter>
                      <Box sx={{ padding: theme.spacing(1, 0) }}>
                        <Image
                          alt="co hoi nghe nghiep"
                          src="/static/images/cards/co-hoi-nghe-nghiep.png"
                          width={110}
                          height={110}
                        ></Image>
                      </Box>
                      <TypographyCenter>
                        {setMessage(Messages.app.shortTitle)} luôn chào đón
                        những nhân tố tài năng và tâm huyết với sứ mệnh {'"'}
                        nâng cao giá trị tri thức, phục vụ hàng triệu người Việt
                        Nam{'"'}
                      </TypographyCenter>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent sx={{ textAlign: 'center' }}>
                      <TypographyCenter variant="h3">
                        Tư vấn & Hỗ trợ
                      </TypographyCenter>
                      <Box sx={{ padding: theme.spacing(1, 0) }}>
                        <Image
                          alt="tu van ho tro"
                          src="/static/images/cards/tu-van-ho-tro.png"
                          width={110}
                          height={110}
                        ></Image>
                      </Box>
                      <TypographyCenter>
                        {setMessage(Messages.app.shortTitle)} sẽ giải đáp mọi
                        câu hỏi của bạn qua: Hotline {Messages.app.hotline}{' '}
                        (8h30-22h000 kể cả T7, CN) và email: {Messages.app.mail}
                      </TypographyCenter>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent sx={{ textAlign: 'center' }}>
                      <TypographyCenter variant="h3">
                        Trở thành Affiliate
                      </TypographyCenter>
                      <Box sx={{ padding: theme.spacing(1, 0) }}>
                        <Image
                          alt="tro thanh affiliate"
                          src="/static/images/cards/tro-thanh-affiliate.png"
                          width={110}
                          height={110}
                        ></Image>
                      </Box>
                      <TypographyCenter>
                        Mọi thông tin hợp tác và giảng dạy xin vui lòng liên hệ
                        với Mr Vương Giám đốc Unica qua Email:{' '}
                        {Messages.app.mail}
                      </TypographyCenter>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                  <Card sx={{ height: '100%' }}>
                    <CardContent sx={{ textAlign: 'center' }}>
                      <TypographyCenter variant="h3">
                        Cơ hội nghề nghiệp
                      </TypographyCenter>
                      <Box sx={{ padding: theme.spacing(1, 0) }}>
                        <Image
                          alt="co hoi nghe nghiep"
                          src="/static/images/cards/co-hoi-nghe-nghiep.png"
                          width={110}
                          height={110}
                        ></Image>
                      </Box>
                      <TypographyCenter>
                        {setMessage(Messages.app.shortTitle)} luôn chào đón
                        những nhân tố tài năng và tâm huyết với sứ mệnh {'"'}
                        nâng cao giá trị tri thức, phục vụ hàng triệu người Việt
                        Nam{'"'}
                      </TypographyCenter>
                    </CardContent>
                  </Card>
                </Grid>
              </GridStrech>
            </Box>
          </Box>
        </Container>
      </Box>
    </>
  );
};

export default Page;
